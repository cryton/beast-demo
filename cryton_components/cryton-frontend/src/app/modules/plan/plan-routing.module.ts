import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreatePlanPageComponent } from './pages/create-plan-page/create-plan-page.component';
import { ListPlansPageComponent } from './pages/list-plans-page/list-plans-page.component';
import { PlanGraphPageComponent } from './pages/plan-graph-page/plan-graph-page.component';
import { PlanYamlPageComponent } from './pages/plan-yaml-page/plan-yaml-page.component';

const routes: Routes = [
  {
    path: 'list',
    component: ListPlansPageComponent
  },
  {
    path: 'create',
    component: CreatePlanPageComponent
  },
  {
    path: ':id/yaml',
    component: PlanYamlPageComponent
  },
  {
    path: ':id/graph',
    component: PlanGraphPageComponent
  },
  {
    path: '',
    redirectTo: '/app/plans/list',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanRoutingModule {}
