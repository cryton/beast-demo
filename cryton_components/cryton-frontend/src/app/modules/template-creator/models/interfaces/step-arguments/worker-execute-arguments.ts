import { SessionArguments } from './session-arguments';

export interface WorkerExecuteArguments extends SessionArguments {
  module: string;
  module_arguments: object;
  create_named_session?: string;
}
