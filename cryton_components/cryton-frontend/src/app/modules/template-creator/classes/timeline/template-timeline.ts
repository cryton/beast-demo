import { Subject } from 'rxjs';
import { NodeTimemark } from 'src/app/shared/classes/node-timemark';
import { Tick } from 'src/app/shared/classes/tick';
import { Timeline } from 'src/app/shared/classes/timeline';
import { TimelineUtils } from 'src/app/shared/classes/timeline-utils';
import { GraphNode } from '../dependency-graph/node/graph-node';
import { StageNode } from '../dependency-graph/node/stage-node';
import { NodeOrganizer } from '../utils/node-organizer';
import { NodeMover } from './node-mover';
import { RegularKonvaTimelineEdge } from './regular-konva-timeline-edge';
import { RegularKonvaTimelineNode } from './regular-konva-timeline-node';
import { RegularTimelineNodeEventConfigurer } from './regular-timeline-node-event-configurer';
import { TimelineEdge } from './timeline-edge';
import { TimelineNode } from './timeline-node';
import { ToolState } from './tool-state';

const NODE_LTICK_NAME = 'timelineNodeLTick';
const NODE_LTICK_TIMEMARK_NAME = 'timelineNodeLTickTimemark';

export class TemplateTimeline extends Timeline {
  nodeMover = new NodeMover(this);
  toolState = new ToolState();

  openNodeParams$ = new Subject<StageNode>();
  selectedNodes = new Set<TimelineNode>();

  private _nodes: TimelineNode[] = [];
  private _leadingTick: Tick;
  private _nodeConfigurer = new RegularTimelineNodeEventConfigurer(this);
  private _nodeOrganizer = new NodeOrganizer('horizontal', false);

  constructor() {
    super(false, true);
  }

  findNodeByGraphNode(graphNode: GraphNode): TimelineNode {
    return this._nodes.find(node => node.graphNode === graphNode);
  }

  /**
   * @returns All nodes from the timeline.
   */
  getNodes(): TimelineNode[] {
    return [...this._nodes];
  }

  /**
   * Adds new node to the plan layer and redraws it.
   *
   * @param node Node to be added.
   */
  addNode(node: TimelineNode): void {
    this._nodes.push(node);
    node.konvaGraphNode = new RegularKonvaTimelineNode(this.theme, node.calcX(), node.name);
    node.configureEvents(this._nodeConfigurer);
    node.addTo(this.mainLayer);
  }

  /**
   * Removes references to a node from the timeline.
   *
   * @param node Node to be removed.
   */
  removeNode(node: TimelineNode): void {
    const nodeIndex = this._nodes.indexOf(node);

    if (nodeIndex !== -1) {
      this._nodes.splice(nodeIndex, 1);
    }
    this.selectedNodes.delete(node);
  }

  /**
   * Visually organizes timeline nodes.
   */
  organizeNodes(): void {
    const bounds = this._nodeOrganizer.organizeNodes(this._nodes);
    const graphHeight = bounds.bottom - bounds.top;

    // Subtract top and bottom padding & subtract top of the bounds to make up for graph's offset from 0.
    this.mainLayer.y(
      (this.height - this.timelinePadding[2]) / 2 + this.timelinePadding[0] - graphHeight / 2 - bounds.top
    );
  }

  /**
   * @returns All edges from the timeline.
   */
  getEdges(): TimelineEdge[] {
    const edges = [];

    this._nodes.forEach(node => edges.push(...node.childEdges));

    return edges;
  }

  /**
   * Creates a new timeline edge between parent and child node.
   *
   * @param parent Parent node.
   * @param child Child node.
   * @returns Created edge.
   */
  createEdge(parent: TimelineNode, child: TimelineNode): TimelineEdge {
    const edge = new TimelineEdge(this, parent, child);
    edge.konvaGraphEdge = new RegularKonvaTimelineEdge(this.theme, parent.konvaGraphNode.getChildEdgePoint());
    edge.konvaGraphEdge.trackWithStart(parent.konvaGraphNode);
    edge.konvaGraphEdge.trackWithEnd(child.konvaGraphNode);

    edge.addTo(this.mainLayer);
    edge.konvaGraphEdge.getKonvaObject().moveToBottom();
    this._nodeOrganizer.organizeGraph(edge.parentNode);
    return edge;
  }

  /**
   * Removes a node between parent and child.
   *
   * @param parent Parent node.
   * @param child Child node.
   */
  removeEdge(parent: TimelineNode, child: TimelineNode): void {
    const edge = this.findEdge(parent, child);
    parent.removeChildEdge(edge);
    child.removeParentEdge(edge);

    if (edge) {
      edge.destroy();
    }
  }

  /**
   * Finds an edge with a specified parent and child.
   *
   * @param parent Parent node.
   * @param child Child node.
   * @returns Edge with a specified parent and child.
   */
  findEdge(parent: TimelineNode, child: TimelineNode): TimelineEdge {
    return this.getEdges().find(edge => edge.parentNode === parent && edge.childNode === child);
  }

  /**
   * Creates the leading tick which shows current delta when dragging a node.
   */
  createLeadingTick(x: number, hasStartTime: boolean): void {
    if (this._leadingTick) {
      throw new Error('Leading tick already exists, destroy the previous one first.');
    }

    const timeMark = new NodeTimemark({
      totalSeconds: TimelineUtils.calcSecondsFromX(this.x, this.getParams()),
      theme: this.theme,
      constantText: hasStartTime ? null : 'No start time',
      useCenterCoords: true,
      timemarkY: 0,
      x,
      name: NODE_LTICK_TIMEMARK_NAME
    });

    timeMark.centerY(this.timelinePadding[0]);

    this._leadingTick = new Tick({
      topY: this.timelinePadding[0],
      bottomY: this.height - this.timelinePadding[2],
      x,
      theme: this.theme,
      isLeading: true,
      strokeWidth: 2,
      listening: false,
      timeMark,
      name: NODE_LTICK_NAME
    });

    this.tickLayer.add(this._leadingTick);
    this.timeMarkLayer.add(timeMark);
    timeMark.moveToTop();
  }

  /**
   * Updates the leading tick position.
   */
  moveLeadingTick(x: number): void {
    if (this._leadingTick) {
      this._leadingTick.x(x);
      (this._leadingTick.timeMark() as NodeTimemark).changeX(x);

      const timelineParams = this.getParams();
      this._leadingTick
        .timeMark()
        .recalculate(TimelineUtils.calcSecondsFromX(x, timelineParams), timelineParams.tickSeconds < 1);
    } else {
      throw new Error('Leading tick does not exist, create it first.');
    }
  }

  destroyLeadingTick(): void {
    if (this._leadingTick) {
      this._leadingTick.timeMark().destroy();
      this._leadingTick.destroy();
    }
    this._leadingTick = null;
  }

  protected _handleTickSecondsChange(): void {
    this._recalculateNodePositions();
    super._handleTickSecondsChange();
  }

  /**
   * Refreshes timeline color theme.
   */
  protected _refreshTheme(): void {
    super._refreshTheme();
    this._nodes.forEach(node => node.konvaGraphNode.setTheme(this.theme));
    this.getEdges().forEach(edge => edge.konvaGraphEdge.setTheme(this.theme));
  }

  /**
   * Initializes konva stage events.
   */
  protected _initKonvaEvents(): void {
    super._initKonvaEvents();
    this.stage.on('click', e => {
      if (!e.evt.shiftKey) {
        this.selectedNodes.forEach(node => node.deselect());
      }
    });
  }

  /**
   * Recalculates all node positions.
   * Has to recalculate positions in order from nodes with smallest x to the biggest
   * because they may depend on each other.
   */
  private _recalculateNodePositions(): void {
    this._nodes
      .sort((a, b) => a.konvaGraphNode.x - b.konvaGraphNode.x)
      .forEach(node => {
        node.updateX();
      });
  }
}
