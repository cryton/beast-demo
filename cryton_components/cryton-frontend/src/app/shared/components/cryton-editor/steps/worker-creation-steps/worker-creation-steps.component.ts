import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { WorkersService } from 'src/app/core/http/worker/workers.service';
import { CrytonEditorStepsComponent } from 'src/app/shared/components/cryton-editor/steps/cryton-editor-steps.component';

@Component({
  selector: 'app-worker-creation-steps',
  templateUrl: './worker-creation-steps.component.html',
  styleUrls: ['./worker-creation-steps.component.scss']
})
export class WorkerCreationStepsComponent extends CrytonEditorStepsComponent implements OnInit, OnDestroy {
  readonly workerForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    force: new FormControl(false)
  });

  constructor(private _workersService: WorkersService) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.eraseEvent$.pipe(takeUntil(this.destroySubject$)).subscribe(() => this.erase());
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  createPostRequest(): void {
    this.create.emit(this._workersService.postItem(this.workerForm.value));
  }

  handleInput(): void {
    this.emitCompletion(this.workerForm.valid);
  }

  erase(): void {
    this.workerForm.reset();
  }
}
