import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[appCrytonTableTitle]'
})
export class CrytonTableTitleDirective implements OnInit {
  constructor(private _elementRef: ElementRef<HTMLElement>) {}

  ngOnInit(): void {
    this._elementRef.nativeElement.classList.add('cryton-table__title');
  }
}
