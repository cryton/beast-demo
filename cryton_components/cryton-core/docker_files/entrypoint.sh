#!/bin/sh

poetry run cryton-core migrate || exit 1
exec "$@"
