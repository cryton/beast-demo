import { EdgeCondition } from '../../../models/interfaces/edge-condition';
import { GraphNode } from '../node/graph-node';
import { GraphEdge } from './graph-edge';

export class StepEdge extends GraphEdge {
  conditions: EdgeCondition[] = [];

  constructor(parentNode: GraphNode) {
    super(parentNode);
  }
}
