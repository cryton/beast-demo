import Konva from 'konva';
import { Node, NodeConfig } from 'konva/lib/Node';
import { Vector2d } from 'konva/lib/types';
import { ShortStringPipe } from 'src/app/shared/pipes/short-string.pipe';
import { StrokeAnimation } from '../../../animations/stroke.animation';
import { Bounds } from '../../../models/interfaces/bounds';
import { Theme } from '../../../models/interfaces/theme';
import { DependencyGraph } from '../dependency-graph';
import { KonvaGraphNode } from '../konva-graph-node';
import { GraphNodeButton } from './buttons/graph-node-button';
import { CompositeNodeEventConfigurer } from './composite-node-event-configurer';
import { GraphNode } from './graph-node';

const NODE_PADDING = 20;
const RECT_STROKE_WIDTH = 3;
const OVERLAY_LINE_WIDTH = 40;
const LABEL_MARGIN = 10;
const LABEL_GAP = 5;
const MAX_TEXT_LENGTH = 25;

export class CompositeKonvaGraphNode implements KonvaGraphNode {
  private _konvaObject: Konva.Group;

  private _subgraphContainer: Konva.Group;
  private _borderRect: Konva.Rect;
  private _topLine: Konva.Line;
  private _bottomLine: Konva.Line;
  private _primaryLabel: Konva.Label;
  private _secondaryLabel: Konva.Label;

  private _shortStringPipe = new ShortStringPipe();
  private _strokeAnimation: StrokeAnimation;

  constructor(
    private _theme: Theme,
    private _primaryText: string,
    private _secondaryText: string,
    private _childDepGraph: DependencyGraph,
    private _button?: GraphNodeButton
  ) {
    this._konvaObject = this._createKonvaObject(
      this._theme,
      this._primaryText,
      this._secondaryText,
      this._childDepGraph
    );
  }

  get x(): number {
    return this._konvaObject.x();
  }
  set x(value: number) {
    this._konvaObject.x(value);
  }

  get y(): number {
    return this._konvaObject.y();
  }
  set y(value: number) {
    this._konvaObject.y(value);
  }

  getKonvaObject(): Node<NodeConfig> {
    return this._konvaObject;
  }

  configureEvents(node: GraphNode, configurer: CompositeNodeEventConfigurer): void {
    configurer.configure(
      node,
      this._primaryLabel,
      this._secondaryLabel,
      this._borderRect,
      this._topLine,
      this._bottomLine
    );
  }

  destroy(): void {
    this._subgraphContainer.destroy();
    this._konvaObject.destroy();
  }

  remove(): void {
    this._konvaObject.remove();
  }

  getWidth(): number {
    return this._borderRect.width();
  }

  getHeight(): number {
    return this._borderRect.height();
  }

  getParentEdgePoint(): Vector2d {
    return { x: this.x + this.getWidth() / 2, y: this.y - 5 }; // -5 makes up for arrow tip.
  }

  getChildEdgePoint(): Vector2d {
    return { x: this.x + this.getWidth() / 2, y: this.y + this.getHeight() };
  }

  setTheme(theme: Theme): void {
    this._theme = theme;
    this._borderRect.stroke(theme.templateCreator.graphNodeRect);
    this._topLine.stroke(theme.templateCreator.graphNodeRect);
    this._bottomLine.stroke(theme.templateCreator.graphNodeRect);
    this._primaryLabel.getTag().fill(theme.primary);
    this._secondaryLabel.getTag().fill(theme.templateCreator.labelBG);
  }

  setPrimaryText(text: string): void {
    this._primaryLabel.getText().text(this._shortStringPipe.transform(text, MAX_TEXT_LENGTH));
    this._secondaryLabel.x(this._primaryLabel.width() + LABEL_GAP);
    this._handleNodeSizing();
  }

  setSecondaryText(text: string): void {
    this._secondaryLabel.getText().text(this._shortStringPipe.transform(text, MAX_TEXT_LENGTH));
    this._handleNodeSizing();
  }

  activateStroke(color: string, moving: boolean): void {
    this._topLine.stroke(color);
    this._bottomLine.stroke(color);

    if (moving) {
      this._strokeAnimation.activate(color);
    } else {
      this._borderRect.stroke(color);
    }
  }

  deactivateStroke(): void {
    this._topLine.stroke(this._theme.templateCreator.graphNodeRect);
    this._bottomLine.stroke(this._theme.templateCreator.graphNodeRect);

    this._strokeAnimation.deactivate();
    this._borderRect.strokeEnabled(true);
    this._borderRect.setAttrs({
      stroke: this._theme.templateCreator.graphNodeRect,
      strokeWidth: RECT_STROKE_WIDTH,
      lineCap: 'round',
      dash: [12, 10],
      cornerRadius: 15
    });
  }

  setDragEnabled(isEnabled: boolean): void {
    this._konvaObject.draggable(isEnabled);
  }

  copy(): KonvaGraphNode {
    const subgraphCopy = this._childDepGraph.copy();
    return new CompositeKonvaGraphNode(this._theme, this._primaryText, this._secondaryText, subgraphCopy, this._button);
  }

  draw(container: Konva.Container): void {
    container.add(this.getKonvaObject());
  }

  private _createKonvaObject(
    theme: Theme,
    primaryText: string,
    secondaryText: string,
    childDepGraph: DependencyGraph
  ): Konva.Group {
    const konvaObject = new Konva.Group();
    this._subgraphContainer = new Konva.Group();

    childDepGraph.draw(this._subgraphContainer, true);

    this._primaryLabel = this._createLabel(primaryText, this._theme.primary, 'white');
    this._secondaryLabel = this._createLabel(secondaryText, this._theme.templateCreator.labelBG, 'black');
    this._secondaryLabel.x(this._primaryLabel.width() + LABEL_GAP);

    this._borderRect = this._createNodeRect(theme);
    this._strokeAnimation = new StrokeAnimation(this._borderRect, konvaObject);

    this._topLine = this._createDashOverlayLine(theme);
    this._bottomLine = this._createDashOverlayLine(theme);

    this._handleNodeSizing();

    // Always add subgraph container last so that it's on top and is interactable.
    konvaObject.add(
      this._borderRect,
      this._topLine,
      this._bottomLine,
      this._primaryLabel,
      this._secondaryLabel,
      this._subgraphContainer
    );

    return konvaObject;
  }

  private _createLabel(text: string, fill: string, color: string): Konva.Label {
    const label = new Konva.Label();
    const tag = new Konva.Tag({
      pointerDirection: 'none',
      fill,
      cornerRadius: 5
    });
    const innerText = new Konva.Text({
      text: this._shortStringPipe.transform(text, MAX_TEXT_LENGTH),
      fontFamily: 'Roboto',
      fontSize: 12,
      padding: 5,
      fill: color
    });

    label.add(tag, innerText);
    label.y(-label.height() - LABEL_MARGIN);

    return label;
  }

  private _handleNodeSizing(): void {
    const minimalWidth = (this._primaryLabel.width() + this._secondaryLabel.width() + LABEL_GAP) * 2 + 2 * NODE_PADDING;

    const subgraphBounds = this._calcDepGraphBounds(this._childDepGraph);
    const subgraphWidth = subgraphBounds.right - subgraphBounds.left;
    const subgraphHeight = subgraphBounds.bottom - subgraphBounds.top;

    this._borderRect.width(Math.max(minimalWidth, subgraphWidth + 2 * NODE_PADDING));
    this._borderRect.height(subgraphHeight + 2 * NODE_PADDING);

    const subgraphXOffset = (this._borderRect.width() - subgraphWidth) / 2;
    this._subgraphContainer.setAttrs({
      x: -subgraphBounds.left + subgraphXOffset,
      y: -subgraphBounds.top + NODE_PADDING
    });

    this._moveDashOverline(this._topLine, this.getWidth() / 2, this._borderRect.y());
    this._moveDashOverline(this._bottomLine, this.getWidth() / 2, this._borderRect.y() + this.getHeight());
  }

  private _calcDepGraphBounds(depGraph: DependencyGraph): Bounds {
    const nodesLeft = depGraph.nodes.map(node => node.konvaGraphNode.x);
    const nodesRight = depGraph.nodes.map(node => node.konvaGraphNode.x + node.konvaGraphNode.getWidth());
    const nodesTop = depGraph.nodes.map(node => node.konvaGraphNode.y);
    const nodesBottom = depGraph.nodes.map(node => node.konvaGraphNode.y + node.konvaGraphNode.getHeight());

    return {
      left: Math.min(...nodesLeft),
      right: Math.max(...nodesRight),
      top: Math.min(...nodesTop),
      bottom: Math.max(...nodesBottom)
    };
  }

  private _createNodeRect(theme: Theme): Konva.Rect {
    return new Konva.Rect({
      stroke: theme.templateCreator.graphNodeRect,
      strokeWidth: RECT_STROKE_WIDTH,
      lineCap: 'round',
      dash: [12, 10],
      cornerRadius: 15
    });
  }

  private _createDashOverlayLine(theme: Theme): Konva.Line {
    return new Konva.Line({
      stroke: theme.templateCreator.graphNodeRect,
      strokeWidth: RECT_STROKE_WIDTH,
      lineCap: 'round'
    });
  }

  private _moveDashOverline(overline: Konva.Line, x: number, y: number): void {
    overline.points([x - OVERLAY_LINE_WIDTH / 2, y, x + OVERLAY_LINE_WIDTH / 2, y]);
  }
}
