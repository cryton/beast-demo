import {
  AbstractControl,
  FormArray,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms';
import { withoutUndefinedAndNull } from 'src/app/shared/utils/without-undefined';
import { parse, stringify } from 'yaml';
import { MsfTriggerParametersComponent } from '../../../components/trigger-parameters/msf-trigger-parameters/msf-trigger-parameters.component';
import { MSFArgs } from '../../../models/interfaces/msf-args';
import { StageNode } from '../../dependency-graph/node/stage-node';
import { validateYaml } from '../../utils/validate-yaml';
import { FormUtils } from './form-utils';
import { TriggerForm } from './trigger-form.interface';

type MSFFormType = FormGroup<{
  exploit: FormControl<string>;
  payload: FormControl<string>;
  identifiers: FormArray<
    FormGroup<{
      key: FormControl<string>;
      value: FormControl<string>;
    }>
  >;
  payload_arguments: FormControl<string>;
  exploit_arguments: FormControl<string>;
}>;

export class MSFForm implements TriggerForm {
  formComponent = MsfTriggerParametersComponent;

  private _triggerArgsForm = new FormGroup({
    exploit: new FormControl<string | null>(null, [Validators.required]),
    payload: new FormControl<string | null>(null, [Validators.required]),
    identifiers: new FormArray<FormGroup<{ key: FormControl<string>; value: FormControl<string> }>>([]),
    payload_arguments: new FormControl<string | null>(null, { validators: [], updateOn: 'blur' }),
    exploit_arguments: new FormControl<string | null>(null, { validators: [], updateOn: 'blur' })
  });

  constructor() {
    const { payload_arguments, exploit_arguments } = this._triggerArgsForm.controls;
    payload_arguments.addValidators(this._yamlValidator());
    exploit_arguments.addValidators(this._yamlValidator());
  }

  getArgsForm(): MSFFormType {
    return this._triggerArgsForm;
  }

  getArgs(): MSFArgs {
    const { identifiers, exploit_arguments, payload_arguments, ...rest } = this._triggerArgsForm.value;
    const exploitArgumentsParsed = exploit_arguments ? parse(exploit_arguments) : null;
    const payloadArgumentsParsed = payload_arguments ? parse(payload_arguments) : null;

    const args: MSFArgs = {
      identifiers: {},
      exploit_arguments: exploitArgumentsParsed,
      payload_arguments: payloadArgumentsParsed,
      ...(rest as { exploit: string; payload: string })
    };

    identifiers.forEach(identifier => (args.identifiers[identifier.key] = identifier.value));
    if (identifiers.length === 0) {
      args.identifiers = null;
    }

    return withoutUndefinedAndNull(args) as MSFArgs;
  }

  isValid(): boolean {
    return this._triggerArgsForm.valid;
  }

  erase(): void {
    this._triggerArgsForm.reset();
    const identifiers = this._triggerArgsForm.controls.identifiers;
    identifiers.clear();
  }

  fill(stage: StageNode): void {
    this._fillMSFForm(this, stage.trigger.getArgs() as MSFArgs);
  }

  copy(): MSFForm {
    const copyForm = new MSFForm();
    this._fillMSFForm(copyForm, this.getArgs());
    return copyForm;
  }

  markAsUntouched(): void {
    this._triggerArgsForm.markAsUntouched();
  }

  isNotEmpty(): boolean {
    return FormUtils.someValueOtherThan(this._triggerArgsForm.value, null);
  }

  addIdentifier(key: string = null, value: string = null): void {
    const identifiers = this._triggerArgsForm.controls.identifiers;
    identifiers.insert(
      0,
      new FormGroup({
        key: new FormControl(key, [Validators.required]),
        value: new FormControl(value, [Validators.required])
      })
    );
  }

  removeIdentifier(index: number): void {
    const identifiers = this._triggerArgsForm.controls.identifiers;
    identifiers.removeAt(index);
  }

  private _fillMSFForm(form: MSFForm, args: MSFArgs): void {
    const { identifiers, exploit_arguments, payload_arguments, ...rest } = args;

    if (identifiers) {
      Object.entries(identifiers).forEach(([key, value]) => form.addIdentifier(key, value));
    }

    form._triggerArgsForm.patchValue({
      exploit_arguments: exploit_arguments ? stringify(exploit_arguments) : null,
      payload_arguments: payload_arguments ? stringify(payload_arguments) : null,
      ...rest
    });
  }

  private _yamlValidator =
    (): ValidatorFn =>
    (control: AbstractControl): ValidationErrors => {
      if (validateYaml(control.value)) {
        return { invalidYaml: true };
      }
      return {};
    };
}
