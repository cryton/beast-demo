export enum StepNodeType {
  EMPIRE_EXECUTE = 'empire/execute',
  EMPIRE_AGENT_DEPLOY = 'empire/agent-deploy',
  WORKER_EXECUTE = 'worker/execute'
}
