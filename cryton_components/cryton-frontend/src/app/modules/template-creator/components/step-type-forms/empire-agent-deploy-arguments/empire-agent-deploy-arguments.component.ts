import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { parse, stringify } from 'yaml';
import { yamlValidator } from '../../../classes/utils/validate-yaml';
import { EmpireAgentDeployArguments } from '../../../models/interfaces/step-arguments/empire-agent-deploy-arguments';
import { StepArgumentsComponent } from '../../../models/interfaces/step-arguments/step-arguments-component';
import { StepArgumentsType } from '../../../models/types/step-node.type';
import { SessionSelectionArgumentsComponent } from '../session-selection-arguments/session-selection-arguments.component';

@Component({
  selector: 'app-empire-agent-deploy-arguments',
  templateUrl: './empire-agent-deploy-arguments.component.html',
  styleUrls: ['../../../styles/step-form.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmpireAgentDeployArgumentsComponent implements StepArgumentsComponent<StepArgumentsType> {
  @ViewChild(SessionSelectionArgumentsComponent) sessionArguments: SessionSelectionArgumentsComponent;

  readonly form = new FormGroup({
    listener_name: new FormControl<string>(null, [Validators.required]),
    listener_port: new FormControl<number>(null, [
      Validators.min(0),
      Validators.max(65535),
      Validators.pattern(/^[0-9]*$/)
    ]),
    listener_options: new FormControl<string>(null, { validators: [yamlValidator()], updateOn: 'blur' }),
    listener_type: new FormControl<string>(null),
    stager_type: new FormControl<string>(null, [Validators.required]),
    stager_options: new FormControl<string>(null, { validators: [yamlValidator()], updateOn: 'blur' }),
    agent_name: new FormControl<string>(null, [Validators.required])
  });

  constructor() {}

  getArguments(): EmpireAgentDeployArguments {
    const sessionArgs = this.sessionArguments?.getArguments();
    const { listener_options, listener_port, stager_options, ...restOfTheArgs } = this.form.getRawValue();

    return {
      listener_port: listener_port ? Number(listener_port) : undefined,
      listener_options: listener_options ? parse(listener_options) : undefined,
      stager_options: stager_options ? parse(stager_options) : undefined,
      ...restOfTheArgs,
      ...sessionArgs
    };
  }

  fill({ listener_options, stager_options, ...restOfTheArgs }: EmpireAgentDeployArguments): void {
    const { session_id, use_any_session_to_target, use_named_session, ssh_connection, ...nonSessionArgs } =
      restOfTheArgs;

    this.form.reset({
      listener_options: listener_options ? stringify(listener_options) : null,
      stager_options: listener_options ? stringify(stager_options) : null,
      ...nonSessionArgs
    });

    this.sessionArguments.fill({ session_id, use_any_session_to_target, use_named_session, ssh_connection });
  }

  isValid(): boolean {
    return this.form.valid && (this.sessionArguments?.isValid() ?? true);
  }
}
