import { TimelineUtils } from 'src/app/shared/classes/timeline-utils';
import { Theme } from '../../models/interfaces/theme';
import { GraphNode } from '../dependency-graph/node/graph-node';
import { StageNode } from '../dependency-graph/node/stage-node';
import { Trigger, TriggerArgs } from '../triggers/trigger';
import { TemplateTimeline } from './template-timeline';

export class TimelineNode extends GraphNode {
  selected = false;

  constructor(public graphNode: StageNode, public timeline: TemplateTimeline, private _theme: Theme) {
    super(graphNode.name, graphNode.note);
  }

  get trigger(): Trigger<TriggerArgs> {
    return this.graphNode.trigger;
  }

  /**
   * Destroys timeline node.
   */
  destroy(): void {
    this.konvaGraphNode.destroy();
  }

  /**
   * Unattaches node from the timeline.
   * Node can still be reattached.
   */
  unattach(): void {
    this.konvaGraphNode.remove();
  }

  /**
   * Updates the X coordinate.
   */
  updateX(): void {
    this.konvaGraphNode.x = this.calcX();
    this.childEdges.forEach(edge => edge.konvaGraphEdge.refreshTracking());
    this.parentEdges.forEach(edge => edge.konvaGraphEdge.refreshTracking());
  }

  /**
   * Highligts the node and sets it as selected.
   */
  select(): void {
    this.timeline.selectedNodes.add(this);
    this.konvaGraphNode.activateStroke(this._theme.accent, false);
    this.konvaGraphNode.getKonvaObject().moveToTop();
    this.selected = true;
  }

  /**
   * Removes node highlight and unsets it at selected.
   */
  deselect(): void {
    this.timeline.selectedNodes.delete(this);
    this.konvaGraphNode.deactivateStroke();
    this.selected = false;
  }

  /**
   * Calculates X coordinate where node should be placed.
   *
   * @returns X coordinate.
   */
  calcX(): number {
    const triggerStart = this.graphNode.trigger.getStartTime();

    return triggerStart != null
      ? TimelineUtils.calcXFromSeconds(triggerStart, this.timeline.getParams())
      : this.timeline.timelinePadding[3];
  }
}
