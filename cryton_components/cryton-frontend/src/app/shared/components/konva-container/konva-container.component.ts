import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  DebugElement,
  HostListener,
  Input,
  OnDestroy,
  ViewChild
} from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { ResizeService } from 'src/app/core/services/resize/resize.service';
import { ThemeService } from 'src/app/core/services/theme/theme.service';
import { KonvaWrapper } from 'src/app/modules/template-creator/classes/konva/konva-wrapper';

@Component({
  selector: 'app-konva-container',
  templateUrl: './konva-container.component.html',
  styleUrls: ['./konva-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class KonvaContainerComponent implements AfterViewInit, OnDestroy {
  @ViewChild('container') canvasContainer: DebugElement;
  @Input() konvaWrapper: KonvaWrapper;

  private readonly _destroy$ = new Subject<void>();

  constructor(private _themeService: ThemeService, private _resizeService: ResizeService) {
    this._resizeService.sidenavResize$
      .pipe(takeUntil(this._destroy$))
      .subscribe(() => this.konvaWrapper.updateDimensions());
  }

  ngAfterViewInit(): void {
    this._initKonva();
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  /**
   * Resizes canvas on window resize
   */
  @HostListener('window:resize') onResize(): void {
    this.konvaWrapper.updateDimensions();
  }

  changeKonvaWrapper(konvaWrapper: KonvaWrapper): void {
    if (this.konvaWrapper) {
      this.konvaWrapper.destroy();
    }

    this.konvaWrapper = konvaWrapper;

    if (this.canvasContainer) {
      this._initKonva();
    }
  }

  private _initKonva(): void {
    this.konvaWrapper.initKonva(this.canvasContainer.nativeElement as HTMLDivElement, this._themeService.currentTheme$);
    this.konvaWrapper.cursorState.container = this.canvasContainer.nativeElement as HTMLDivElement;
  }
}
