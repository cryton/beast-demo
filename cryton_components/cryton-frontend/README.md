# Cryton Frontend

## Description
Cryton Frontend is a graphical interface used to interact with [Cryton Core](https://gitlab.ics.muni.cz/beast-public/cryton/cryton-core) (its API).

To be able to execute attack scenarios, you also need to install **[Cryton Core](https://gitlab.ics.muni.cz/beast-public/cryton/cryton-core)** 
and **[Cryton Worker](https://gitlab.ics.muni.cz/beast-public/cryton/cryton-worker)** tools.

Cryton toolset is tested and targeted primarily on **Debian** and **Kali Linux**. Please keep in mind that 
**only the latest version is supported** and issues regarding different OS or distributions may **not** be resolved.

## Settings
Before installation, you need to set the environment variables to match **Cryton Core** settings. Environment variables 
can be found in `src/environments/`. For production build, modify the _environment.prod.ts_ file, else modify the _environment.ts_ file.

**crytonRESTApiHost:** REST API host (localhost by default).

**crytonRESTApiPort:** REST API port (8000 by default).

**refreshDelay:** Sets artificial delay in milliseconds for refresh API requests, users usually react better if the 
requests don't happen instantly, but they can see a tiny bit of loading. Initial API request doesn't use delay, 
this is only for refreshing data (300 milliseconds by default).

## Installation (using Docker Compose)
Cryton Frontend can be installed using Docker Compose.

### Requirements
- [Docker Compose](https://docs.docker.com/compose/install/)

Add yourself to the group *docker*, so you can work with Docker CLI without *sudo*:
```shell
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
docker run hello-world
```

### Installing and running with Docker Compose
First, we have to clone the repo and switch to the correct version (if you haven't already).
```shell
git clone https://gitlab.ics.muni.cz/beast-public/cryton/cryton-frontend.git
cd cryton-frontend
```

Make sure you've correctly set the [settings](#settings). You can't change the settings on a running container.

We are now ready to build and start the frontend:
```shell
docker compose up -d --build
```

Docker Compose will automatically build the app for production with minimal dependencies and deploy it on a Nginx 
server inside the container. The default port is set to **8080**, you can change this setting in the `docker-compose.yml` file.

## Installation (manual)

### Requirements
- [npm](https://nodejs.org/en/)

### Installing manually using npm

1.  Clone this repository and cd into it.
2.  Run `npm install`, npm will take care of installing all the dependencies.
3.  Based on the needs you can:
    - Serve the app by running `ng serve` or `ng serve --prod` for production settings.
        - Only use `ng serve` for development/testing, in a real production environment use either docker installation or 
      production build deployed on a production-ready web server (for example Nginx).
        - App will now be available on **localhost:4200**
        - To change the port use argument `--port [port]`
    - Build the app by running `npm run build` or `npm run build-prod` for production.
        - You can find the build in the **/dist** folder.

## Development

### Requirements
- [npm](https://nodejs.org/en/)

### Installing and running with npm

- App uses husky to run pre-commit hooks. These include:
    - Code formatting with Prettier.
    - Linting with ESLint.
    - Running unit tests with Karma.
- To start development:
    1. Install dependencies with `npm install`.
    2. Run `npm start` to run the development server. The app will now listen for changes and refresh itself on every change in the project's filesystem.

## Usage
**Please keep in mind that the [Cryton Core](https://gitlab.ics.muni.cz/beast-public/cryton/cryton-core) 
must be running and its API must be reachable.**

Use in-app help pages to learn about usage.
