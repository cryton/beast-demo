import { CdkAccordionModule } from '@angular/cdk/accordion';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SharedModule } from 'src/app/shared/shared.module';
import { CodeEditorModule } from '../code-editor/code-editor.module';
import { ExecutionReportCardComponent } from './components/execution-report-card/execution-report-card.component';
import { ExecutionVariablePreviewComponent } from './components/execution-variable-preview/execution-variable-preview.component';
import { ExecutionVariableUploaderComponent } from './components/execution-variable-uploader/execution-variable-uploader.component';
import { ExecutionVariableComponent } from './components/execution-variable/execution-variable.component';
import { PostponeRunComponent } from './components/postpone-run/postpone-run.component';
import { ReportTimelineHelpComponent } from './components/report-timeline-help/report-timeline-help.component';
import { RunReportCardComponent } from './components/run-report-card/run-report-card.component';
import { StageReportCardComponent } from './components/stage-report-card/stage-report-card.component';
import { StepReportCardComponent } from './components/step-report-card/step-report-card.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { CreateRunPageComponent } from './pages/create-run-page/create-run-page.component';
import { ListRunsPageComponent } from './pages/list-runs-page/list-runs-page.component';
import { RunYamlPageComponent } from './pages/run-yaml-page/run-yaml-page.component';
import { RunPageComponent } from './pages/run/run-page.component';
import { RunRoutingModule } from './run-routing.module';

@NgModule({
  declarations: [
    ExecutionVariableComponent,
    ExecutionVariableUploaderComponent,
    RunPageComponent,
    ExecutionReportCardComponent,
    StageReportCardComponent,
    ReportTimelineHelpComponent,
    RunReportCardComponent,
    StepReportCardComponent,
    TimelineComponent,
    PostponeRunComponent,
    ExecutionVariablePreviewComponent,
    CreateRunPageComponent,
    ListRunsPageComponent,
    RunYamlPageComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MatIconModule,
    MatButtonModule,
    MatDividerModule,
    MatProgressBarModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    CdkAccordionModule,
    MatDialogModule,
    MatTooltipModule,
    MatSelectModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDialogModule,
    RunRoutingModule,
    MatNativeDateModule,
    CodeEditorModule
  ],
  exports: [TimelineComponent]
})
export class RunModule {}
