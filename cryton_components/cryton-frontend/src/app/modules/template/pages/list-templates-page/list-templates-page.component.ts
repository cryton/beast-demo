import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TemplateService } from 'src/app/core/http/template/template.service';
import { Template } from 'src/app/models/api-responses/template.interface';
import { renderComponentTrigger } from 'src/app/shared/animations/render-component.animation';
import { DeleteButton } from 'src/app/shared/components/cryton-table/buttons/delete-button';
import { LinkButton } from 'src/app/shared/components/cryton-table/buttons/link-button';
import { TableButton } from 'src/app/shared/components/cryton-table/buttons/table-button';
import { CrytonTableComponent } from 'src/app/shared/components/cryton-table/cryton-table.component';
import { TemplatesTableDataSource } from 'src/app/shared/components/cryton-table/data-source/templates-table.data-source';

@Component({
  selector: 'app-list-templates-page',
  templateUrl: './list-templates-page.component.html',
  animations: [renderComponentTrigger]
})
export class ListTemplatesPageComponent implements OnInit {
  @ViewChild(CrytonTableComponent)
  templatesTable: CrytonTableComponent<Template>;

  dataSource: TemplatesTableDataSource;
  buttons: TableButton[];

  constructor(private _templateService: TemplateService, private _dialog: MatDialog) {}

  ngOnInit(): void {
    this.dataSource = new TemplatesTableDataSource(this._templateService);
    this.buttons = [
      new LinkButton('Show YAML', 'description', '/app/templates/:id/yaml'),
      new DeleteButton<Template>(this._templateService, this._dialog)
    ];
  }
}
