import { RunState } from '../types/run-state.type';

export interface Run {
  id: number;
  created_at: string | null;
  updated_at: string | null;
  state: RunState;
  start_time: string | null;
  pause_time: string | null;
  finish_time: string | null;
  schedule_time: string | null;
  aps_job_id: string;
  plan_model: number;
}
