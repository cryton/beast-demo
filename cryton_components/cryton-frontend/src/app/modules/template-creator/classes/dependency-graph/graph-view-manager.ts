import { Vector2d } from 'konva/lib/types';
import { Bounds } from '../../models/interfaces/bounds';
import { KonvaWrapper } from '../konva/konva-wrapper';
import { DependencyGraph } from './dependency-graph';
import { CANVAS_PADDING } from './dependency-graph-constants';
import { GraphNode } from './node/graph-node';

const MIN_SCALE = 0.1;
const MAX_SCALE = 1.5;

export class GraphViewManager {
  constructor() {}

  fitScreen(wrapper: KonvaWrapper, depGraph: DependencyGraph): void {
    const stage = wrapper.stage;

    if (!stage || !depGraph.nodes) {
      return;
    }

    stage.x(0).y(0);

    const bounds = depGraph.getGraphBounds();
    const boundingRectCenter = this._getBoundsCenter(depGraph.getGraphBounds());

    // Rescale stage to fit the whole view with dependency graph.
    const scale = this._calcRescale(
      wrapper,
      bounds.right - bounds.left + CANVAS_PADDING,
      bounds.bottom - bounds.top + CANVAS_PADDING
    );
    stage.scale({ x: scale, y: scale });

    // Calculate centering vector which moves the graph to the center of the view.
    // The center of the canvas gets moved by the scale factor, hence we need to rescale it to its original position.
    const centeringVector: Vector2d = {
      x: (stage.width() / 2) * (1 / scale) - boundingRectCenter.x,
      y: (stage.height() / 2) * (1 / scale) - boundingRectCenter.y
    };

    this._centerNodes(depGraph.nodes, centeringVector);
  }

  zoomIn(wrapper: KonvaWrapper): void {
    this._rescaleStage(wrapper, 0.1);
  }

  zoomOut(wrapper: KonvaWrapper): void {
    this._rescaleStage(wrapper, -0.1);
  }

  /**
   * Rescales the stage by the increment.
   *
   * @param wrapper Konva wrapper for the dependency graph.
   * @param increment Scale increment.
   */
  private _rescaleStage(wrapper: KonvaWrapper, increment: number): void {
    const newScale = wrapper.stage.scale().x + increment;

    if (newScale >= MIN_SCALE && newScale <= MAX_SCALE) {
      wrapper.stage.scale({ x: newScale, y: newScale });
    }
  }

  private _centerNodes(nodes: GraphNode[], centeringVector: Vector2d): void {
    for (const node of nodes) {
      const konvaNode = node.konvaGraphNode;

      konvaNode.x = konvaNode.x + centeringVector.x;
      konvaNode.y = konvaNode.y + centeringVector.y;
    }

    // Fix edge points after automatic node movement.
    nodes.forEach(node => node.childEdges.forEach(edge => edge.konvaGraphEdge.refreshTracking()));
  }

  /**
   * Calculates the scale factor needed to fit the dependency graph inside the canvas.
   *
   * @param wrapper Konva wrapper for the dependency graph.
   * @param rectWidth Width of the bounding rectangle.
   * @param rectHeight Height of the bounding rectangle.
   */
  private _calcRescale(wrapper: KonvaWrapper, rectWidth: number, rectHeight: number): number {
    const xRescale = wrapper.stage.width() / rectWidth;
    const yRescale = wrapper.stage.height() / rectHeight;
    const minRescale = Math.min(xRescale, yRescale);

    if (minRescale > 1) {
      return 1;
    } else if (minRescale <= MIN_SCALE) {
      return MIN_SCALE;
    }

    return minRescale;
  }

  /**
   * Returns the center of the bounding rectangle.
   *
   * @param bounds Bounding rectangle of nodes.
   */
  private _getBoundsCenter(bounds: Bounds): Vector2d {
    const boundsHeight = bounds.bottom - bounds.top;
    const boundsWidth = bounds.right - bounds.left;

    return {
      x: bounds.left + boundsWidth / 2,
      y: bounds.top + boundsHeight / 2
    };
  }
}
