import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatFormFieldHarness } from '@angular/material/form-field/testing';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectHarness } from '@angular/material/select/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SessionArguments } from '../../../models/interfaces/step-arguments/session-arguments';
import { SshConnectionArgumentsComponent } from '../ssh-connection-arguments/ssh-connection-arguments.component';
import { SshConnectionArgumentsHarness } from '../ssh-connection-arguments/ssh-connection-arguments.harness';
import { SessionSelectionArgumentsComponent } from './session-selection-arguments.component';

describe('SessionSelectionArgumentsComponent', () => {
  let component: SessionSelectionArgumentsComponent;
  let fixture: ComponentFixture<SessionSelectionArgumentsComponent>;
  let loader: HarnessLoader;

  const getSessionSelection = (): Promise<MatSelectHarness> => loader.getHarness(MatSelectHarness);
  const getFieldByLabel = (label: string): Promise<MatFormFieldHarness> =>
    loader.getHarness(MatFormFieldHarness.with({ floatingLabelText: label }));
  const getSSHForm = (): Promise<SshConnectionArgumentsHarness> => loader.getHarness(SshConnectionArgumentsHarness);

  const testArgumentsFlow = (args: SessionArguments): void => {
    component.fill(args);
    expect(component.getArguments()).toEqual(args);
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatFormFieldModule, ReactiveFormsModule, NoopAnimationsModule, MatSelectModule, MatInputModule],
      declarations: [SessionSelectionArgumentsComponent, SshConnectionArgumentsComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionSelectionArgumentsComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Display tests', () => {
    it('should display session selection by default', async () => {
      const sessionSelection = await getSessionSelection();
      expect(sessionSelection).toBeDefined();
    });

    it('should display any session to target field', async () => {
      const sessionSelection = await getSessionSelection();
      await sessionSelection.clickOptions({ text: 'Use any session to target' });
      fixture.detectChanges();

      const field = await getFieldByLabel('Use any session to target *');
      expect(field).toBeDefined();
    });

    it('should display named session field', async () => {
      const sessionSelection = await getSessionSelection();
      await sessionSelection.clickOptions({ text: 'Use named session' });
      fixture.detectChanges();

      const field = await getFieldByLabel('Use any named session *');
      expect(field).toBeDefined();
    });

    it('should display session ID field', async () => {
      const sessionSelection = await getSessionSelection();
      await sessionSelection.clickOptions({ text: 'Use session ID' });
      fixture.detectChanges();

      const field = await getFieldByLabel('Session ID *');
      expect(field).toBeDefined();
    });

    it('should display SSH connection fields', async () => {
      const sessionSelection = await getSessionSelection();
      await sessionSelection.clickOptions({ text: 'Use SSH connection' });
      fixture.detectChanges();

      const sshForm = await getSSHForm();
      expect(sshForm).toBeDefined();
    });
  });

  describe('Arguments tests', () => {
    it('should not fill arguments where more than 1 session is defined', () => {
      const args1: SessionArguments = { session_id: 1, use_any_session_to_target: 'a' };
      const args2: SessionArguments = { use_named_session: 'a', ssh_connection: { target: 'a' } };
      const args3: SessionArguments = { session_id: 1, use_named_session: 'a', use_any_session_to_target: 'a' };
      const invalidArgs = [args1, args2, args3];

      invalidArgs.forEach(args => {
        expect(() => component.fill(args)).toThrowError('Session arguments can only define 1 type of session.');
      });
    });

    it('should fill & return session ID', () => {
      testArgumentsFlow({ session_id: 1 });
    });

    it('should fill & return use named session', () => {
      testArgumentsFlow({ use_named_session: 'a' });
    });

    it('should fill & return use any session to target', () => {
      testArgumentsFlow({ use_any_session_to_target: 'a' });
    });

    it('should fill & return ssh connection', () => {
      testArgumentsFlow({
        ssh_connection: { target: '127.0.0.1', username: 'root', password: 'root', port: 81, ssh_key: '15' }
      });
    });
  });

  describe('isRequired tests', () => {
    it('should be invalid if empty when isRequired = true', () => {
      component.isRequired = true;
      expect(component.isValid()).toBeFalse();
    });

    it('should be valid if empty when isRequired = false', () => {
      component.isRequired = false;
      expect(component.isValid()).toBeTrue();
    });

    it(`should add option 'None' when isRequired = false`, async () => {
      component.isRequired = false;
      const sessionSelection = await getSessionSelection();

      expect(await sessionSelection.getOptions({ text: 'None' })).toBeDefined();
    });
  });
});
