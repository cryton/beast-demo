import Konva from 'konva';
import { RippleAnimation } from '../../animations/ripple.animation';
import { Theme } from '../../models/interfaces/theme';

export const CONNECTOR_NAME = 'nodeConnector';
export const CONNECTOR_CIRCLE_NAME = 'nodeConnectorCircle';

export class NodeConnector {
  private _konvaObject: Konva.Group;
  private _connectorCircle: Konva.Circle;
  private _rippleCircle: Konva.Circle;
  private _rippleAnimation: RippleAnimation;

  constructor(nodeWidth: number, nodeHeight: number) {
    this._createKonvaObject(nodeWidth / 2, nodeHeight);
    this._rippleAnimation = new RippleAnimation(this._rippleCircle);
  }

  getKonvaObject(): Konva.Group {
    return this._konvaObject;
  }

  /**
   * Animates ripple around the connector.
   *
   * @param maxRadius Maximal radius the ripple can reach.
   * @param defaultOpacity Opacity in the default state.
   */
  animateRipple(maxRadius: number, defaultOpacity: number): void {
    this._rippleAnimation.animate(maxRadius, defaultOpacity);
  }

  /**
   * Unanimates the ripple if the animation didn't end properly.
   */
  unanimateRipple(): void {
    this._rippleAnimation.unanimate();
  }

  changeTheme(theme: Theme): void {
    this._connectorCircle.fill(theme.primary);
  }

  private _createKonvaObject(x: number, y: number): void {
    this._konvaObject = new Konva.Group({ name: CONNECTOR_NAME });
    this._rippleCircle = new Konva.Circle({
      radius: 0,
      fill: '#979797',
      opacity: 0.2,
      x,
      y
    });

    this._connectorCircle = new Konva.Circle({
      name: CONNECTOR_CIRCLE_NAME,
      radius: 7,
      fill: '#ff5543',
      hitStrokeWidth: 10,
      x,
      y
    });

    this._konvaObject.add(this._rippleCircle);
    this._konvaObject.add(this._connectorCircle);
  }
}
