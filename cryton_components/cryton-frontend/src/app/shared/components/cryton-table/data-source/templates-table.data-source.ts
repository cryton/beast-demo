import { Template } from 'src/app/models/api-responses/template.interface';
import { CrytonTableDataSource } from 'src/app/shared/components/cryton-table/data-source/cryton-table.datasource';
import { Column } from '../models/column.interface';

export class TemplatesTableDataSource extends CrytonTableDataSource<Template> {
  columns: Column[] = [
    {
      name: 'id',
      display: 'ID',
      highlight: false,
      filterable: true,
      sortable: true
    },
    {
      name: 'file',
      display: 'FILE',
      highlight: false,
      filterable: true,
      sortable: true
    }
  ];
  displayFunctions = [];
  highlightDictionary = {};
}
