from django.test import TestCase
from unittest.mock import patch, MagicMock, call, Mock, mock_open
import os
import yaml
import datetime
import pytz
from model_bakery import baker
import copy

from cryton_core.lib.util import util, exceptions

from cryton_core.cryton_app.models import WorkerModel

TESTS_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))


@patch('cryton_core.lib.util.util.logger.logger', util.logger.structlog.getLogger('cryton-core-debug'))
@patch('amqpstorm.Connection', Mock())
class TestUtil(TestCase):

    def setUp(self) -> None:
        self.worker = baker.make(WorkerModel)

    def test_convert_to_utc(self):
        original_datetime = datetime.datetime.now()
        ret = util.convert_to_utc(original_datetime)
        self.assertEqual(ret, original_datetime)

    def test_convert_to_utc_localized(self):
        original_datetime = pytz.timezone('utc').localize(datetime.datetime.now())
        ret = util.convert_to_utc(original_datetime, offset_aware=True)
        self.assertEqual(ret, original_datetime)

    def test_convert_from_utc(self):
        original_datetime = datetime.datetime.now()
        ret = util.convert_from_utc(original_datetime, 'utc')
        self.assertEqual(ret, original_datetime)

    def test_convert_from_utc_localized(self):
        original_datetime = pytz.timezone('utc').localize(datetime.datetime.now())
        ret = util.convert_from_utc(original_datetime, 'utc', True)
        self.assertEqual(ret, original_datetime)

    def test_IgnoreNestedUndefined_getattr(self):
        res = util.IgnoreNestedUndefined(name="a")
        res = res.__getattr__("b")
        assert res.__str__() == '{{ a.b }}'

    def test_IgnoreNestedUndefined_getitem(self):
        res = util.IgnoreNestedUndefined(name="a")
        res = res.__getitem__("0")
        assert res.__str__() == '{{ a[0] }}'

    def test_fill_template(self):
        with open(TESTS_DIR + '/plan-template.yaml') as plan_yaml:
            plan_template = plan_yaml.read()
        with open(TESTS_DIR + '/inventory.yaml') as inventory:
            plan_inventory = yaml.safe_load(inventory)

        filled = util.fill_template(plan_template, plan_inventory)

        self.assertIsInstance(filled, str)

    def test_fill_template_error(self):
        with self.assertRaises(exceptions.ValidationError):
            util.fill_template("foo: {{ unfilled + 14 }}", {})

    def test_fill_execution_variables(self):
        result = util.fill_execution_variables(
            {"var_int": "{{ int_var }}", "var_str": "{{ str_var }}", "var_bool": "{{ bool_var }}",
             "var_list": "{{ list_var[0] }}"},
            {"int_var": 1, "str_var": "string", "bool_var": True, "list_var": [1]})

        assert result == {"var_int": 1, "var_str": "string", "var_bool": True, "var_list": 1}

    def test_fill_execution_variables_error(self):
        with self.assertRaises(exceptions.StepValidationError):
            util.fill_execution_variables({"undefined": "{{ undefined_var }}"}, {})

    # TODO: once we switch to pytest, uncomment this
    # @pytest.mark.parametrize(
    #     "p_file_data, p_parsed_data",
    #     [
    #         ("test: configuration", {"test": "configuration"}),
    #         ("{\"test\": \"configuration\"}", {"test": "configuration"}),
    #         ("[SECTION]\ntest: configuration", {"SECTION": {"test": "configuration"}}),
    #     ]
    # )
    # def test_read_config(self, mocker, p_file_data, p_parsed_data):
    #
    #     result = util.parse_inventory_file(p_file_data)
    #
    #     assert result == p_parsed_data
    #
    # def test_read_config_error(self, mocker):
    #     mocker.patch('builtins.open', mock_open(read_data=': :'))
    #
    #     with pytest.raises(ValueError):
    #         util.parse_inventory_file("")

    def test_split_into_lists(self):
        input_list = [1, 2, 3]
        ret = util.split_into_lists(input_list, 3)
        self.assertEqual(ret, [[1], [2], [3]])

        input_list = [1, 2, 3, 4]
        ret = util.split_into_lists(input_list, 3)
        self.assertEqual(ret, [[1, 2], [3], [4]])

        input_list = [1, 2]
        ret = util.split_into_lists(input_list, 3)
        self.assertEqual(ret, [[1], [2], []])

    # @patch("cryton_core.lib.util.util.amqpstorm.Connection")
    @patch("cryton_core.lib.util.util.run_step_executions")
    @patch("cryton_core.lib.util.util.split_into_lists", Mock(return_value=["exec1", "exec2", "exec3"]))
    @patch("cryton_core.lib.util.util.Thread")
    def test_run_executions_in_threads(self, mock_thread, mock_run_step_executions):
        mock_conn = MagicMock()
        mock_connection = MagicMock()
        mock_connection.return_value.__enter__.return_value = mock_conn
        patch_connection = patch("cryton_core.lib.util.util.amqpstorm.Connection", mock_connection)
        patch_connection.start()

        util.run_executions_in_threads(["exec1", "exec2", "exec3"])

        mock_thread.assert_has_calls([
            call(target=mock_run_step_executions, args=(mock_conn, "exec1")),
            call(target=mock_run_step_executions, args=(mock_conn, "exec2")),
            call(target=mock_run_step_executions, args=(mock_conn, "exec3"))]
        )

    def test_run_step_executions(self):
        step_exec1 = MagicMock()
        step_exec2 = MagicMock()
        connection = MagicMock()
        channel = MagicMock()

        connection.channel.return_value.__enter__.return_value = channel

        util.run_step_executions(connection, [step_exec1, step_exec2])

        step_exec1.execute.assert_called_with(channel)
        step_exec2.execute.assert_called_with(channel)

    def test_parse_dot_argument(self):
        test_arg = "[te[1]st[s][][1]"
        result = util.parse_dot_argument(test_arg)
        self.assertEqual(["[te[1]st[s][]", "[1]"], result)

    def test_parse_dot_argument_no_index(self):
        test_arg = "test"
        result = util.parse_dot_argument(test_arg)
        self.assertEqual([test_arg], result)

    @patch("cryton_core.lib.util.util.parse_dot_argument")
    def test_get_from_mod_in(self, mock_parse_dot_arg):
        mock_parse_dot_arg.side_effect = [["parent"], ["output"], ["username"]]
        resp = util.get_from_dict({'parent': {'output': {'username': 'admin'}}}, '$parent.output.username')
        self.assertEqual(resp, 'admin')

        mock_parse_dot_arg.side_effect = [["a", "[1]"], ["c"]]
        resp = util.get_from_dict({'a': [{'b': 1}, {'c': 2}]}, '$a[1].c')
        self.assertEqual(resp, 2)

    def test_update_inner(self):
        mod_in = {'parent': {'t1': {'t2': 666}}}

        args = {
            'arg1': 1,
            'arg2': {
                'test': '$parent.t1.t2'
            },
            'arg3': [1, 2, 3],
            'arg4': [
                {1: '$parent.test;'}
            ],
            'arg5': {
                '1': {
                    '2': '$parent.test;'
                }
            }
        }

        util.update_inner(args, mod_in, '$parent')

        self.assertEqual(args.get('arg2').get('test'), 666)
        self.assertEqual(args.get('arg4')[0].get(1), '$parent.test;')
        self.assertEqual(args.get('arg5').get('1').get('2'), '$parent.test;')

    def test_replace_value_in_dict(self):
        mod_in = {'t1': {'t2': 666}}

        args = {
            'arg1': 1,
            'arg2': {
                'test': '$parent.t1.t2'
            },
            'arg3': [1, 2, 3],
            'arg4': [
                {1: '$parent.test;'}
            ],
            'arg5': {
                '1': {
                    '2': '$parent.test;'
                }
            }
        }
        util.replace_value_in_dict(copy.deepcopy(args), mod_in)
        with self.assertRaises(ValueError):
            util.replace_value_in_dict(copy.deepcopy(args), mod_in, '$testing')
        # Ok
        util.replace_value_in_dict(copy.deepcopy(args), None)
        with self.assertRaises(ValueError):
            util.replace_value_in_dict(None, mod_in)
        util.replace_value_in_dict(copy.deepcopy(args), {})

    def test_rename_key(self):
        dict_in = {'1': 1, '2': 2, '3': {'4': {'5': 6}, '7': 8}}
        rename_from = '3.4'
        rename_to = '9.10.11'
        expected = {'1': 1, '2': 2, '3': {'7': 8}, '9': {'10': {'11': {'5': 6}}}}

        util.rename_key(dict_in, rename_from, rename_to)
        self.assertEqual(dict_in, expected)

        dict_in = {'1': 1, '2': 2}
        rename_from = '2'
        rename_to = '6'
        expected = {'1': 1, '6': 2}

        util.rename_key(dict_in, rename_from, rename_to)
        self.assertEqual(dict_in, expected)

        dict_in = {'1': 1, '2': 2}
        rename_from = '3'
        rename_to = '6'

        with self.assertRaises(KeyError):
            util.rename_key(dict_in, rename_from, rename_to)

    @patch("cryton_core.lib.util.util.open", mock_open(read_data="line1 \nline2 \n"))
    def test_get_logs(self):
        result = util.get_logs()
        self.assertEqual(['line1', 'line2'], result)
