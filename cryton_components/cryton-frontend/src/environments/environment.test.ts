export const environment = {
  production: false,
  refreshDelay: 0,
  useHttps: false,
  crytonRESTApiHost: 'localhost',
  crytonRESTApiPort: 8000
};
