export enum CreateStageRoute {
  STAGE_PARAMS = 'stage_params',
  DEP_GRAPH = 'dep_graph'
}

export enum CreateTemplateRoute {
  TEMPLATE_PARAMS = 'template_params',
  DEP_GRAPH = 'dep_graph',
  TIMELINE = 'timeline'
}
