#!/usr/bin/python
import json
import traceback
import subprocess
from typing import Optional, Union
from schema import Schema, Optional as SchemaOptional, SchemaError, Or

from cryton_worker.lib.util.module_util import Metasploit, OUTPUT, SERIALIZED_OUTPUT, RETURN_CODE


def validate(arguments: dict) -> int:
    """
    Validate input values for the execute function

    :param arguments: Arguments for module execution
    :return: 0 If arguments are valid
    :raises: Schema Exception
    """
    conf_schema = Schema(
        {
            "cmd": str,
            SchemaOptional("end_checks"): list,
            SchemaOptional("timeout"): int,
            SchemaOptional("session_id"): Or(int, str),
            SchemaOptional(SERIALIZED_OUTPUT): bool,
        },
    )

    conf_schema.validate(arguments)

    return 0


def execute(arguments: dict) -> dict:
    """
    Takes arguments in form of dictionary and runs command based on them.

    :param arguments: Arguments from which is compiled and ran command
    :return: Module output containing:
                return_code (0-success, 1-fail),
                output (raw output),
                serialized_process_output (output that can be used in other modules)

    """
    ret_vals = {
        RETURN_CODE: -1,
        OUTPUT: "",
        SERIALIZED_OUTPUT: {}
    }

    try:
        validate(arguments)
    except SchemaError as err:
        ret_vals[OUTPUT] = str(err)
        return ret_vals

    session_id: Union[int, str, None] = arguments.get("session_id")
    cmd: str = arguments.get("cmd")
    end_checks: Optional[list] = arguments.get("end_checks")
    timeout: Optional[int] = arguments.get("timeout")
    serialized_output: bool = arguments.get(SERIALIZED_OUTPUT, False)

    if session_id is not None:
        if Metasploit().is_connected() is False:
            ret_vals[OUTPUT] = "Could not connect to msfrpc, is msfrpc running?"
            return ret_vals

        try:
            msf = Metasploit()
            process_output = msf.execute_in_session(cmd, str(session_id), timeout, end_checks)
        except Exception as ex:
            ret_vals[OUTPUT] = str(ex)
            return ret_vals

        ret_vals.update({RETURN_CODE: 0})

    else:
        try:
            process = subprocess.run(cmd, timeout=timeout, capture_output=True, shell=True)
            process.check_returncode()
            process_output = process.stdout.decode("utf-8")
            process_error = process.stderr.decode("utf-8")

        except subprocess.TimeoutExpired:
            ret_vals[OUTPUT] = "Command execution timed out"
            return ret_vals
        except subprocess.CalledProcessError as err:  # exited with return code other than 0
            ret_vals[OUTPUT] = err.stderr.decode("utf-8")
            return ret_vals
        except Exception as ex:
            tb = traceback.format_exc()
            process_output = f"{ex} {tb}"
            ret_vals[OUTPUT] = process_output
            return ret_vals

        process_output = f"{process_output} {chr(10)} {process_error}"
        if not process_error:
            ret_vals[RETURN_CODE] = 0

    ret_vals[OUTPUT] = process_output

    if serialized_output:
        try:
            cmd_serialized_output = json.loads(process_output)
            ret_vals[SERIALIZED_OUTPUT] = cmd_serialized_output
        except (json.JSONDecodeError, TypeError):
            ret_vals[OUTPUT] += "serialized_output_error: Output of the script is not valid JSON."
            ret_vals[RETURN_CODE] = -1

    return ret_vals
