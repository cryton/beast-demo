import { RunState } from '../types/run-state.type';

export const runStateColorMap = new Map<RunState, string>([
  ['PAUSED', 'down'],
  ['PAUSING', 'down'],
  ['RUNNING', 'running'],
  ['PENDING', 'pending'],
  ['WAITING', 'pending'],
  ['AWAITING', 'pending'],
  ['STARTING', 'pending'],
  ['FINISHED', 'success'],
  ['SCHEDULED', 'pending'],
  ['ERROR', 'error'],
  ['IGNORE', 'error'],
  ['TERMINATED', 'error'],
  ['TERMINATING', 'error']
]);
