import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { parse, stringify } from 'yaml';
import { yamlValidator } from '../../../classes/utils/validate-yaml';
import { StepArgumentsComponent } from '../../../models/interfaces/step-arguments/step-arguments-component';
import { WorkerExecuteArguments } from '../../../models/interfaces/step-arguments/worker-execute-arguments';
import { StepArgumentsType } from '../../../models/types/step-node.type';
import { SessionSelectionArgumentsComponent } from '../session-selection-arguments/session-selection-arguments.component';

@Component({
  selector: 'app-worker-execute-arguments',
  templateUrl: './worker-execute-arguments.component.html',
  styleUrls: ['../../../styles/step-form.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkerExecuteArgumentsComponent implements StepArgumentsComponent<StepArgumentsType> {
  @ViewChild(SessionSelectionArgumentsComponent) sessionSelectionArgs?: SessionSelectionArgumentsComponent;

  readonly form = new FormGroup({
    module: new FormControl<string>(null, [Validators.required]),
    module_arguments: new FormControl<string>(null, {
      validators: [Validators.required, yamlValidator()],
      updateOn: 'blur'
    }),
    create_named_session: new FormControl(null)
  });

  constructor() {}

  getArguments(): WorkerExecuteArguments {
    const { module_arguments, ...restOfTheArgs } = this.form.getRawValue();
    const sessionArgs = this.sessionSelectionArgs?.getArguments();

    return {
      module_arguments: module_arguments ? parse(module_arguments) : undefined,
      ...restOfTheArgs,
      ...sessionArgs
    };
  }

  fill({ module_arguments, ...restOfTheArgs }: WorkerExecuteArguments): void {
    const { module, create_named_session, ...sessionArgs } = restOfTheArgs;

    this.form.reset({ module, create_named_session });

    if (module_arguments) {
      this.form.get('module_arguments').setValue(stringify(module_arguments));
    }

    this.sessionSelectionArgs.fill(sessionArgs);
  }

  isValid(): boolean {
    return this.form.valid && (this.sessionSelectionArgs?.isValid() ?? false);
  }
}
