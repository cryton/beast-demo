import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SharedModule } from 'src/app/shared/shared.module';
import { PlansTableComponent } from './components/plans-table/plans-table.component';
import { RunsTableComponent } from './components/runs-table/runs-table.component';
import { WorkerTableComponent } from './components/worker-table/worker-table.component';
import { WorkersListComponent } from './components/workers-list/workers-list.component';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';

@NgModule({
  declarations: [
    PlansTableComponent,
    RunsTableComponent,
    DashboardPageComponent,
    WorkersListComponent,
    WorkerTableComponent
  ],
  imports: [
    CommonModule,
    MatTabsModule,
    SharedModule,
    MatPaginatorModule,
    MatIconModule,
    MatDividerModule,
    MatProgressBarModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatButtonModule
  ]
})
export class DashboardModule {}
