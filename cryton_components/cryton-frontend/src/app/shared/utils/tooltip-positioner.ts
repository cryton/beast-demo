import { Vector2d } from 'konva/lib/types';
import { TooltipOrientation } from '../components/cryton-tooltip/cryton-tooltip.component';

// Minimal distance of tooltip from an edge.
const MIN_TOOLTIP_DIST = 200;

export class TooltipPositioner {
  constructor() {}

  /**
   * Caluculates best tooltip orientation to fit the tooltip inside the stage.
   *
   * @param clickPos X an Y position of tooltip (where user clicked).
   * @param defaultOrientation Default orientation to return if tooltip isn't close to no edge.
   * @returns Tuple in format [horizontal orientation, vertial orientation].
   */
  calcOrientation(
    containerSize: { width: number; height: number },
    clickPos: Vector2d,
    defaultOrientation: TooltipOrientation
  ): TooltipOrientation {
    const closeToLeft = clickPos.x < MIN_TOOLTIP_DIST;
    const closeToRight = containerSize.width - clickPos.x < MIN_TOOLTIP_DIST;
    const closeToTop = clickPos.y < MIN_TOOLTIP_DIST;
    const closeToBottom = containerSize.height - clickPos.y < MIN_TOOLTIP_DIST;

    const horizontalPos = closeToLeft ? 'right' : closeToRight ? 'left' : 'middle';
    const verticalPos = closeToTop ? 'bottom' : closeToBottom ? 'top' : 'middle';

    if (horizontalPos === 'middle' && verticalPos === 'middle') {
      return defaultOrientation;
    }
    return [horizontalPos, verticalPos];
  }
}
