export class FormUtils {
  static someValueDefined(object: object): boolean {
    return Object.values(object).some(value => value);
  }

  static someValueOtherThan(object: object, value: string | number): boolean {
    return Object.values(object).some(itemValue => itemValue !== value);
  }
}
