import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlanService } from 'src/app/core/http/plan/plan.service';
import { AlertService } from 'src/app/core/services/alert/alert.service';

@Component({
  selector: 'app-plan-yaml-page',
  template:
    '<app-yaml-preview [resourceService]="planService" itemName="plan" [pluckArgs]="pluckArgs" [keepLastArg]="false"></app-yaml-preview>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlanYamlPageComponent {
  pluckArgs = ['detail'];

  constructor(protected _route: ActivatedRoute, public planService: PlanService, protected _alert: AlertService) {}
}
