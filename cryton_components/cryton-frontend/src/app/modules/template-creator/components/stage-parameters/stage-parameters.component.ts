import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ThemeService } from 'src/app/core/services/theme/theme.service';
import { ComponentInputDirective } from 'src/app/shared/directives/component-input.directive';
import { StageNode } from '../../classes/dependency-graph/node/stage-node';
import { StageForm } from '../../classes/stage-creation/forms/stage-form';
import { TimelineNode } from '../../classes/timeline/timeline-node';
import { Trigger, TriggerArgs } from '../../classes/triggers/trigger';
import { TriggerFactory } from '../../classes/triggers/trigger-factory';
import { DeltaDependency, DeltaDependencyFinder } from '../../classes/utils/delta-dependency-finder';
import { TriggerType } from '../../models/enums/trigger-type';
import { TemplateCreatorStateService } from '../../services/template-creator-state.service';
import { getControlError } from './stage-parameters.errors';

type Option = { value: string; name: string };

@Component({
  selector: 'app-stage-parameters',
  templateUrl: './stage-parameters.component.html',
  styleUrls: ['./stage-parameters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StageParametersComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(ComponentInputDirective) triggerFormHost: ComponentInputDirective;

  triggerTypeOpts: Option[] = [
    { value: 'delta', name: 'Delta' },
    { value: 'HTTPListener', name: 'HTTPListener' },
    { value: 'datetime', name: 'DateTime' },
    { value: 'MSFListener', name: 'MSFListener' }
  ];
  stageFormGroup: FormGroup;

  private _destroy$ = new Subject<void>();
  private _stageForm: StageForm;
  private _initialized = false;
  private _triggerChangeSub: Subscription;

  constructor(
    private _cd: ChangeDetectorRef,
    private _tcState: TemplateCreatorStateService,
    private _themeService: ThemeService
  ) {}

  get valid(): boolean {
    return this.stageForm.isValid();
  }

  get stageForm(): StageForm {
    return this._stageForm;
  }

  @Input() set stageForm(value: StageForm) {
    if (value) {
      this._stageForm = value;
    } else {
      this._stageForm = new StageForm();
    }

    this.stageFormGroup = this.stageForm.getStageArgsForm();

    if (this._initialized) {
      this.renderTrigger();
      this._createTriggerChangeSub(value);
    }
  }

  ngOnInit(): void {
    if (!this.stageForm) {
      this.stageForm = new StageForm();
    }
    this.stageFormGroup = this.stageForm.getStageArgsForm();
  }

  ngAfterViewInit(): void {
    this.renderTrigger();
    this._createTriggerChangeSub(this.stageForm);
    this._initialized = true;
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  setEditedNodeName(name: string): void {
    this.stageForm.editedNodeName = name;
  }

  fillFromStage(stage: StageNode): void {
    this.stageForm.fill(stage);
  }

  /**
   * Gets from control error message.
   *
   * @param controlName Form control name.
   * @param formGroup Form group which control belongs to.
   * @returns Current error message.
   */
  getControlError(controlName: string, formGroup: FormGroup): string {
    return getControlError(formGroup, controlName);
  }

  /**
   * Edits stage passed in an argument with stage form parameters
   * and current stage creation dependency graph.
   *
   * @param stage Stage to edit.
   */
  editStage(stage: StageNode): void {
    const { name, triggerType } = this.stageForm.getStageArgs();
    const trigger = TriggerFactory.createTrigger(triggerType, this.stageForm.getTriggerArgs());
    stage.name = name;
    stage.note = trigger.getType();

    try {
      this._editTrigger(stage, trigger);
    } catch (e) {
      throw e;
    }

    const timelineNode = this._tcState.timeline.findNodeByGraphNode(stage);

    if (timelineNode) {
      timelineNode.name = name;
    }
  }

  renderTrigger(): void {
    const triggerParamsComponent = this.stageForm.getTriggerFormComponent();
    const viewContainerRef = this.triggerFormHost.viewContainerRef;

    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(triggerParamsComponent);
    const componentInstance = componentRef.instance;

    componentInstance.triggerForm = this.stageForm.getTriggerForm();
    this._cd.detectChanges();
  }

  /**
   * Edits stage's trigger.
   *
   * @param trigger New trigger.
   */
  private _editTrigger(stage: StageNode, trigger: Trigger<TriggerArgs>): void {
    const timelineNode = this._tcState.timeline.findNodeByGraphNode(stage);

    if (trigger.getType() === TriggerType.DELTA) {
      if (!timelineNode) {
        this._handleChangeToDelta(stage, trigger);
      } else {
        stage.editTrigger(trigger);
        timelineNode?.updateX();
      }
    } else if (timelineNode) {
      this._handleChangeToNonDelta(stage, timelineNode, trigger);
    } else {
      stage.editTrigger(trigger);
    }
  }

  /**
   * Handles change from delta to non-delta trigger.
   * Recreates all timeline edges as needed.
   *
   * @param trigger Non-delta trigger.
   */
  private _handleChangeToNonDelta(stage: StageNode, timelineNode: TimelineNode, trigger: Trigger<TriggerArgs>): void {
    this._destroyTimelineEdges(stage);
    timelineNode.destroy();
    this._tcState.timeline.removeNode(timelineNode);

    const parents = DeltaDependencyFinder.getParents(stage);
    const parentDepsBefore = DeltaDependencyFinder.getChildDependenciesOfStages(parents);

    stage.editTrigger(trigger);

    const parentDepsAfter = DeltaDependencyFinder.getChildDependenciesOfStages(parents);
    const addedDependencies = DeltaDependencyFinder.filterAddedDependencies(parentDepsBefore, parentDepsAfter);

    addedDependencies.forEach(dependency => {
      const parentTimelineNode = this._tcState.timeline.findNodeByGraphNode(dependency.parent);
      const childTimelineNode = this._tcState.timeline.findNodeByGraphNode(dependency.child);
      this._tcState.timeline.createEdge(parentTimelineNode, childTimelineNode);
    });
  }

  /**
   * Handles change from non-delta trigger to delta trigger.
   * Recreates all timeline edges as needed.
   *
   * @param trigger Delta trigger.
   */
  private _handleChangeToDelta(stage: StageNode, trigger: Trigger<TriggerArgs>): void {
    const timelineNode = new TimelineNode(stage, this._tcState.timeline, this._themeService.currentTheme);
    this._tcState.timeline.addNode(timelineNode);

    const parents = DeltaDependencyFinder.getParents(stage);
    const parentDepsBefore = DeltaDependencyFinder.getChildDependenciesOfStages(parents);

    stage.editTrigger(trigger);

    const parentDepsAfter = DeltaDependencyFinder.getChildDependenciesOfStages(parents);
    const removedDependencies = DeltaDependencyFinder.filterRemovedDependencies(parentDepsBefore, parentDepsAfter);
    const addedDependencies = DeltaDependencyFinder.filterAddedDependencies(parentDepsBefore, parentDepsAfter);
    addedDependencies.push(...DeltaDependencyFinder.getChildDependencies(stage));

    removedDependencies.forEach(dependency => {
      const parentTimelineNode = this._tcState.timeline.findNodeByGraphNode(dependency.parent);
      const childTimelineNode = this._tcState.timeline.findNodeByGraphNode(dependency.child);
      this._tcState.timeline.removeEdge(parentTimelineNode, childTimelineNode);
    });
    addedDependencies.forEach(dependency => {
      const parentTimelineNode = this._tcState.timeline.findNodeByGraphNode(dependency.parent);
      const childTimelineNode = this._tcState.timeline.findNodeByGraphNode(dependency.child);
      this._tcState.timeline.createEdge(parentTimelineNode, childTimelineNode);
    });
  }

  /**
   * Destroys timeline edges of this node.
   */
  private _destroyTimelineEdges(stage: StageNode): void {
    const dependencies: DeltaDependency[] = DeltaDependencyFinder.getDependencies(stage);

    dependencies.forEach(dependency => {
      const parentTimelineNode = this._tcState.timeline.findNodeByGraphNode(dependency.parent);
      const childTimelineNode = this._tcState.timeline.findNodeByGraphNode(dependency.child);
      this._tcState.timeline.removeEdge(parentTimelineNode, childTimelineNode);
    });
  }

  private _createTriggerChangeSub(stageForm: StageForm): void {
    if (this._triggerChangeSub) {
      this._triggerChangeSub.unsubscribe();
    }

    this._triggerChangeSub = stageForm.triggerTypeChange$
      .pipe(takeUntil(this._destroy$))
      .subscribe(() => this.renderTrigger());
  }
}
