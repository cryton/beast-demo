export interface EventConfigurer {
  configure(...args): void;

  removeOwnListeners(...args): void;

  removeAllListeners(...args): void;
}
