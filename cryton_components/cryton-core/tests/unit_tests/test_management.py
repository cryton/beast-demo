import pytest
from unittest.mock import Mock, patch
from pytest_mock import MockerFixture

from cryton_core.lib.util import logger
from cryton_core.cryton_app.management.commands import runserver, startlistener, startgunicorn  # , startmonitoring


@patch('cryton_core.lib.util.logger.logger', logger.structlog.getLogger('cryton-core-test'))
class TestCommandRunServer:
    path = "cryton_core.cryton_app.management.commands.runserver"

    @pytest.fixture
    def f_logger_object(self, mocker: MockerFixture):
        return mocker.patch(self.path + ".logger_object")

    @pytest.fixture
    def f_original_handle(self, mocker: MockerFixture):
        return mocker.patch(self.path + ".CommandRunserver.handle")

    @pytest.fixture
    def f_thread(self, mocker: MockerFixture):
        return mocker.patch(self.path + ".Thread")

    def test_handle(self, f_original_handle, f_thread, f_logger_object):
        runserver.Command().handle()

        f_thread.return_value.start.assert_called_once()
        f_original_handle.assert_called_once()
        f_logger_object.log_queue.put.assert_called_once_with(None)

    def test_handle_error(self, f_original_handle, f_thread, f_logger_object):
        f_original_handle.side_effect = RuntimeError

        with pytest.raises(RuntimeError):
            runserver.Command().handle()

        f_thread.return_value.start.assert_called_once()
        f_original_handle.assert_called_once()
        f_logger_object.log_queue.put.assert_called_once_with(None)


@patch('cryton_core.lib.util.logger.logger', logger.structlog.getLogger('cryton-core-test'))
class TestCommandStartListener:
    path = "cryton_core.cryton_app.management.commands.startlistener"

    def test_handle(self, mocker):
        mock_listener: Mock = mocker.patch(self.path + ".Listener")

        startlistener.Command().handle()
        mock_listener.return_value.start.assert_called()


@patch('cryton_core.lib.util.logger.logger', logger.structlog.getLogger('cryton-core-test'))
class TestCommandStartMonitoring:
    path = "cryton_core.cryton_app.management.commands.startmonitoring"

    @pytest.mark.skip(reason="this feature is not available at the moment")
    def test_handle(self, mocker):
        pass


@patch('cryton_core.lib.util.logger.logger', logger.structlog.getLogger('cryton-core-test'))
class TestStartGunicorn:
    path = "cryton_core.cryton_app.management.commands.startgunicorn"

    @pytest.fixture
    def f_logger_object(self, mocker: MockerFixture):
        return mocker.patch(self.path + ".logger_object")

    @pytest.fixture
    def f_thread(self, mocker: MockerFixture):
        return mocker.patch(self.path + ".Thread")

    @pytest.fixture
    def f_gunicorn_application(self, mocker: MockerFixture):
        return mocker.patch(self.path + ".GunicornApplication")

    @pytest.fixture
    def f_altered_gunicorn_application(self):
        return startgunicorn.GunicornApplication(Mock(), {"workers": 2})

    def test_gunicorn_application___init__(self):
        mock_app = Mock()
        mock_options = {"workers": 2}

        gunicorn_application = startgunicorn.GunicornApplication(mock_app, mock_options)

        assert gunicorn_application.application == mock_app
        assert gunicorn_application.options == mock_options
        assert gunicorn_application.cfg.settings["workers"].value == mock_options["workers"]

    def test_gunicorn_application_init(self, f_altered_gunicorn_application):
        f_altered_gunicorn_application.init(Mock(), Mock(), Mock())

    def test_gunicorn_application_load(self, f_altered_gunicorn_application):
        mock_app = Mock()

        f_altered_gunicorn_application.application = mock_app

        result = f_altered_gunicorn_application.load()

        assert result == mock_app

    def test_command_add_arguments(self):
        mock_parser = Mock()

        startgunicorn.Command().add_arguments(mock_parser)

        assert mock_parser.add_argument.call_count == 2

    def test_command_handle(self, f_gunicorn_application, f_thread, f_logger_object):
        startgunicorn.Command().handle()

        f_thread.return_value.start.assert_called_once()
        f_gunicorn_application.return_value.run.assert_called_once()
        f_logger_object.log_queue.put.assert_called_once_with(None)

    def test_command_handle_error(self, f_gunicorn_application, f_thread, f_logger_object):
        f_gunicorn_application.return_value.run.side_effect = RuntimeError

        with pytest.raises(RuntimeError):
            startgunicorn.Command().handle()

        f_thread.return_value.start.assert_called_once()
        f_gunicorn_application.return_value.run.assert_called_once()
        f_logger_object.log_queue.put.assert_called_once_with(None)
