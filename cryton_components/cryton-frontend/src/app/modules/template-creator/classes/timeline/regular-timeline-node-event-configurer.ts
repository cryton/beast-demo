import Konva from 'konva';
import { Circle } from 'konva/lib/shapes/Circle';
import { TemplateTimeline } from './template-timeline';
import { TimelineNode } from './timeline-node';
import { TimelineNodeEventConfigurer } from './timeline-node-event-configurer';

export class RegularTimelineNodeEventConfigurer implements TimelineNodeEventConfigurer {
  constructor(private _wrapper: TemplateTimeline) {}

  configure(node: TimelineNode, nodeCircle: Circle, primaryLabel: Konva.Label): void {
    this.removeOwnListeners(node, nodeCircle, primaryLabel);
    this._initNodeSelectEvent(node, nodeCircle);
    this._initGroupEvents(node);
  }

  removeOwnListeners(node: TimelineNode, nodeCircle: Konva.Circle, primaryLabel: Konva.Label): void {
    const konvaObject = node.konvaGraphNode.getKonvaObject();

    nodeCircle.off('click.regular-timeline-node');
    konvaObject.off('dragstart.regular-timeline-node');
    konvaObject.off('dragmove.regular-timeline-node');
    konvaObject.off('dragend.regular-timeline-node');
    konvaObject.off('dblclick.regular-timeline-node');
  }

  removeAllListeners(node: TimelineNode, nodeCircle: Konva.Circle, primaryLabel: Konva.Label): void {
    node.konvaGraphNode.getKonvaObject().off();
    nodeCircle.off();
    primaryLabel.off();
  }

  /**
   * Creates a shift + click event for selection and deselection of a node.
   */
  private _initNodeSelectEvent(node: TimelineNode, nodeCircle: Circle): void {
    nodeCircle.on('click.regular-timeline-node', e => {
      if (e.evt.shiftKey) {
        if (this._wrapper.selectedNodes.has(node)) {
          node.deselect();
        } else {
          node.select();
        }
      }
    });
  }

  /**
   * Creates the main konva group and initializes events on it.
   */
  private _initGroupEvents(node: TimelineNode): void {
    const konvaObject = node.konvaGraphNode.getKonvaObject();

    konvaObject.dragBoundFunc(pos => this._wrapper.nodeMover.nodeDragFunc(node, pos));

    konvaObject.on('dragstart.regular-timeline-node', () => {
      if (!this._wrapper.toolState.isVerticalMoveEnabled) {
        this._wrapper.createLeadingTick(node.konvaGraphNode.x, node.graphNode.trigger.getStartTime() != undefined);
      }
    });
    konvaObject.on('dragmove.regular-timeline-node', () => {
      if (!this._wrapper.toolState.isVerticalMoveEnabled) {
        this._wrapper.moveLeadingTick(node.konvaGraphNode.x);
      }
      konvaObject.moveToTop();
    });
    konvaObject.on('dragend.regular-timeline-node', () => {
      this._wrapper.destroyLeadingTick();
    });
    konvaObject.on('dblclick.regular-timeline-node', () => {
      this._wrapper.openNodeParams$.next(node.graphNode);
    });
  }
}
