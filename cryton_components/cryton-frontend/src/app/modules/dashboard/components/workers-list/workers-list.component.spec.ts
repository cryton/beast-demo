import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BehaviorSubject } from 'rxjs';
import { WorkersService } from 'src/app/core/http/worker/workers.service';
import { AlertService } from 'src/app/core/services/alert/alert.service';
import { Worker } from 'src/app/models/api-responses/worker.interface';
import { WorkerTableComponent } from 'src/app/modules/dashboard/components/worker-table/worker-table.component';
import { WorkerTableHarness } from 'src/app/modules/dashboard/components/worker-table/worker-table.harness';
import { SharedModule } from 'src/app/shared/shared.module';
import { workers } from 'src/app/testing/mockdata/workers.mockdata';
import { alertServiceStub } from 'src/app/testing/stubs/alert-service.stub';
import { Spied } from 'src/app/testing/utility/utility-types';
import { WorkersListComponent } from './workers-list.component';

describe('WorkersListComponent', () => {
  let component: WorkersListComponent;
  let fixture: ComponentFixture<WorkersListComponent>;
  let loader: HarnessLoader;

  const workers$ = new BehaviorSubject({ count: workers.length, results: workers });

  const workersServiceStub = jasmine.createSpyObj('WorkersService', ['fetchItems']) as Spied<WorkersService>;
  workersServiceStub.fetchItems.and.returnValue(workers$.asObservable());

  const compareWorkerWithData = async (workerTable: WorkerTableHarness, data: Worker): Promise<void> => {
    expect(await workerTable.getID()).toBe(data.id);
    expect(await workerTable.getState()).toBe(data.state);
    expect(await workerTable.getName()).toBe(data.name);
    expect(await workerTable.getDescription()).toBe(data.description);
  };

  const compareWorkersWithData = async (workerTables: WorkerTableHarness[], data: Worker[]): Promise<void> => {
    expect(workerTables.length).toBe(data.length);

    for (let i = 0; i < workerTables.length; i++) {
      await compareWorkerWithData(workerTables[i], data[i]);
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [WorkersListComponent, WorkerTableComponent],
      imports: [
        MatButtonModule,
        MatIconModule,
        BrowserAnimationsModule,
        MatPaginatorModule,
        SharedModule,
        MatTooltipModule,
        MatProgressBarModule,
        MatDividerModule
      ],
      providers: [
        { provide: WorkersService, useValue: workersServiceStub },
        { provide: AlertService, useValue: alertServiceStub }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    workers$.next({ count: workers.length, results: workers });
    fixture = TestBed.createComponent(WorkersListComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show fetched workers', async () => {
    const workerTables = await loader.getAllHarnesses(WorkerTableHarness);

    expect(workerTables.length).toBe(workers.length);
    compareWorkersWithData(workerTables, workers);
  });

  it('should reload fetched workers', async () => {
    let workerTables = await loader.getAllHarnesses(WorkerTableHarness);
    expect(workerTables.length).toBe(workers.length);
    compareWorkerWithData(workerTables[0], workers[0]);

    workers$.next({ count: 1, results: [workers[0]] });
    const reloadBtn = await loader.getHarness(MatButtonHarness.with({ text: 'refresh' }));
    await reloadBtn.click();
    fixture.detectChanges();
    workerTables = await loader.getAllHarnesses(WorkerTableHarness);

    compareWorkersWithData(workerTables, [workers[0]]);
  });
});
