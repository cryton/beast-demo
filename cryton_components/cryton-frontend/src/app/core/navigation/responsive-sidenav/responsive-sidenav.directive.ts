import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Directive, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Subject, takeUntil } from 'rxjs';
import { ResizeService } from '../../services/resize/resize.service';

export const SIDENAV_COMPACT_LOCALSTORAGE = 'isSidenavCompact';
export const SIDENAV_CLASS = 'sidenav';
export const SIDENAV_ANIMATION_DURATION = 200;

@Directive({
  selector: '[appResponsiveSidenav]',
  exportAs: 'responsiveSidenav'
})
export class ResponsiveSidenavDirective implements OnInit, OnDestroy {
  private _destroy$ = new Subject<void>();

  constructor(
    private _host: MatSidenav,
    private _elementRef: ElementRef<HTMLElement>,
    private _breakpointObserver: BreakpointObserver,
    private _resizeService: ResizeService
  ) {}

  get isCompact(): boolean {
    return JSON.parse(localStorage.getItem(SIDENAV_COMPACT_LOCALSTORAGE)) as boolean;
  }

  get isOver(): boolean {
    return this._host.mode === 'over';
  }

  ngOnInit(): void {
    this.initSidebarBreakpoint();
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  /**
   * Initializes breakpoint observer to switch sidenav mode based on viewport max width.
   */
  initSidebarBreakpoint(): void {
    this._breakpointObserver
      .observe(['(max-width: 768px)'])
      .pipe(takeUntil(this._destroy$))
      .subscribe((breakpointState: BreakpointState) => this._onBreakpointHit(breakpointState.matches));
  }

  /**
   * Toggles visibility or display of sidenav based on current sidenav mode.
   */
  toggleSidenav(): void {
    this._waitForAnimationAndEmitResize();

    if (this.isOver) {
      this._host.toggle();
    } else {
      const isCompact = !this.isCompact;
      localStorage.setItem(SIDENAV_COMPACT_LOCALSTORAGE, JSON.stringify(isCompact));
      this._onCompactToggle(isCompact);
    }
  }

  private _onCompactToggle(isCompact: boolean): void {
    if (isCompact) {
      this._elementRef.nativeElement.classList.add(SIDENAV_CLASS + '--compact');
    } else {
      this._elementRef.nativeElement.classList.remove(SIDENAV_CLASS + '--compact');
    }
  }

  private _onBreakpointHit(breakpointMatches: boolean): void {
    let isCompact = JSON.parse(localStorage.getItem(SIDENAV_COMPACT_LOCALSTORAGE)) as boolean;

    if (isCompact === undefined || isCompact === null) {
      localStorage.setItem(SIDENAV_COMPACT_LOCALSTORAGE, String(true));
      isCompact = true;
    }

    if (breakpointMatches) {
      this._host.mode = 'over';
      this._onCompactToggle(false);

      if (this._host) {
        this._host.close().then(() => {
          this._waitForAnimationAndEmitResize();
        });
      }
    } else {
      this._host.mode = 'side';
      this._onCompactToggle(isCompact);

      if (this._host) {
        this._host.open().then(() => {
          this._waitForAnimationAndEmitResize();
        });
      }
    }
  }

  private _waitForAnimationAndEmitResize(): void {
    setTimeout(() => {
      this._resizeService.sidenavResize$.next();
    }, SIDENAV_ANIMATION_DURATION);
  }
}
