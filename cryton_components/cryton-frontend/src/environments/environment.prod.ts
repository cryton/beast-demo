export const environment = {
  production: true,
  refreshDelay: 300,
  useHttps: false,
  crytonRESTApiHost: 'localhost',
  crytonRESTApiPort: 8000
};
