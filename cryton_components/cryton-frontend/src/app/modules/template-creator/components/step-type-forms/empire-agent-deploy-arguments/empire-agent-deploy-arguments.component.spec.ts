import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatFormFieldHarness } from '@angular/material/form-field/testing';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CodeEditorModule } from 'src/app/modules/code-editor/code-editor.module';
import { stringify } from 'yaml';
import { EmpireAgentDeployArguments } from '../../../models/interfaces/step-arguments/empire-agent-deploy-arguments';
import { SessionSelectionArgumentsComponent } from '../session-selection-arguments/session-selection-arguments.component';
import { EmpireAgentDeployArgumentsComponent } from './empire-agent-deploy-arguments.component';

const EXPECTED_DEFAULT_FIELD_LABELS: string[] = [
  'Listener name *',
  'Listener port',
  'Listener options (in YAML)',
  'Listener type',
  'Stager type *',
  'Stager options (in YAML)',
  'Agent name *',
  'Session type *'
];

const MOCK_ARGUMENTS: EmpireAgentDeployArguments = {
  listener_name: 'Listener name',
  listener_port: 80,
  listener_options: { option: 'value' },
  listener_type: 'http',
  stager_type: 'multi/bash',
  stager_options: { option: 'value' },
  agent_name: 'Agent name',
  session_id: 8
};

describe('EmpireAgentDeployArgumentsComponent', () => {
  let component: EmpireAgentDeployArgumentsComponent;
  let fixture: ComponentFixture<EmpireAgentDeployArgumentsComponent>;
  let loader: HarnessLoader;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        NoopAnimationsModule,
        MatSelectModule,
        CodeEditorModule
      ],
      declarations: [EmpireAgentDeployArgumentsComponent, SessionSelectionArgumentsComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpireAgentDeployArgumentsComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create all available default fields', async () => {
    const fields = await loader.getAllHarnesses(MatFormFieldHarness);
    const fieldLabels = await Promise.all(fields.map(field => field.getLabel()));

    expect(fieldLabels.sort()).toEqual([...EXPECTED_DEFAULT_FIELD_LABELS].sort());
  });

  it('should fill arguments', async () => {
    component.fill(MOCK_ARGUMENTS);
    const { session_id, stager_options, listener_options, ...defaultArgs } = MOCK_ARGUMENTS;
    const stringifiedArgs = {
      stager_options: stringify(stager_options),
      listener_options: stringify(listener_options),
      ...defaultArgs
    };

    expect(component.form.value).toEqual(stringifiedArgs);
    expect(component.sessionArguments.getArguments()).toEqual({ session_id });
  });

  it('should return arguments', async () => {
    component.fill(MOCK_ARGUMENTS);
    expect(component.getArguments()).toEqual(MOCK_ARGUMENTS);
  });
});
