import Konva from 'konva';
import { Vector2d } from 'konva/lib/types';
import { StrokeAnimation } from '../../../animations/stroke.animation';
import { Theme } from '../../../models/interfaces/theme';
import { EventConfigurer } from '../event-configurer';
import { KonvaGraphNode } from '../konva-graph-node';
import { KonvaGraphEdge } from './konva-graph-edge';

const POINTER_LENGTH = 10;
const POINTER_WIDTH = 7;
const STROKE_WIDTH = 3;
const HIT_STROKE_WIDTH = 15;
const GRAPH_EDGE_NAME = 'graphEdge';

export class RegularKonvaGraphEdge implements KonvaGraphEdge {
  private _konvaObject: Konva.Arrow;
  private _strokeAnimation: StrokeAnimation;

  private _startTrackedNode: KonvaGraphNode;
  private _endTrackedNode: KonvaGraphNode;

  constructor(theme: Theme, initialPoint: Vector2d) {
    this._createKonvaObject(theme, initialPoint);
  }

  public get start(): Vector2d {
    const points = this._konvaObject.points();
    return { x: points[0], y: points[1] };
  }

  public set start(value: Vector2d) {
    const points = this._konvaObject.points();
    points[0] = value.x;
    points[1] = value.y;
    this._konvaObject.points(points);
  }

  public get end(): Vector2d {
    const points = this._konvaObject.points();
    return { x: points[2], y: points[3] };
  }

  public set end(value: Vector2d) {
    const points = this._konvaObject.points();
    points[2] = value.x;
    points[3] = value.y;
    this._konvaObject.points(points);
  }

  trackWithStart(node: KonvaGraphNode): void {
    const konvaObject = node.getKonvaObject();

    if (this._startTrackedNode) {
      this._startTrackedNode.getKonvaObject().off('dragmove');
    }

    this._startTrackedNode = node;

    this.start = node.getChildEdgePoint();
    konvaObject.on('dragmove', () => {
      this.start = node.getChildEdgePoint();
    });
  }

  trackWithEnd(node: KonvaGraphNode): void {
    const konvaObject = node.getKonvaObject();

    if (this._endTrackedNode) {
      this._endTrackedNode.getKonvaObject().off('dragmove');
    }

    this._endTrackedNode = node;

    this.end = node.getParentEdgePoint();
    konvaObject.on('dragmove', () => {
      this.end = node.getParentEdgePoint();
    });
  }

  getKonvaObject(): Konva.Node {
    return this._konvaObject;
  }

  configureEvents(configurer: EventConfigurer): void {
    configurer.configure(this._konvaObject);
  }

  destroy(): void {
    this._konvaObject.destroy();
  }

  setTheme(theme: Theme): void {
    this._konvaObject.stroke(theme.templateCreator.graphEdge);
    this._konvaObject.fill(theme.templateCreator.graphEdge);
  }

  activateStroke(color: string, moving: boolean): void {
    if (moving) {
      this._strokeAnimation.activate(color);
    } else {
      this._konvaObject.stroke(color);
      this._konvaObject.fill(color);
    }
  }

  deactivateStroke(): void {
    this._strokeAnimation.deactivate();
  }

  refreshTracking(): void {
    this.start = this._startTrackedNode.getChildEdgePoint();
    this.end = this._endTrackedNode.getParentEdgePoint();
  }

  draw(container: Konva.Container): void {
    container.add(this.getKonvaObject());
  }

  /**
   * Initializes edge konva object.
   */
  private _createKonvaObject(theme: Theme, initialPoint: Vector2d): void {
    this._konvaObject = new Konva.Arrow({
      pointerLength: POINTER_LENGTH,
      strokeWidth: STROKE_WIDTH,
      pointerWidth: POINTER_WIDTH,
      points: [initialPoint.x, initialPoint.y, initialPoint.x, initialPoint.y],
      hitStrokeWidth: HIT_STROKE_WIDTH,
      name: GRAPH_EDGE_NAME,
      shadowForStrokeEnabled: false
    });
    this.setTheme(theme);
    this._strokeAnimation = new StrokeAnimation(this._konvaObject, this._konvaObject);
  }
}
