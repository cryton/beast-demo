import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SharedModule } from 'src/app/shared/shared.module';
import { LogRoutingModule } from './log-routing.module';
import { ListLogsPageComponent } from './pages/list-logs-page/list-logs-page.component';

@NgModule({
  declarations: [ListLogsPageComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatFormFieldModule,
    MatDividerModule,
    MatPaginatorModule,
    SharedModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatInputModule,
    LogRoutingModule
  ]
})
export class LogModule {}
