import { ComponentHarness } from '@angular/cdk/testing';

export class SshConnectionArgumentsHarness extends ComponentHarness {
  static hostSelector = 'app-ssh-connection-arguments';
}
