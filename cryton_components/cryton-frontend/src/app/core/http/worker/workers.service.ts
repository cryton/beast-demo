import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WorkerHealth } from '../../../models/api-responses/worker-health.interface';
import { Worker } from '../../../models/api-responses/worker.interface';
import { Endpoint } from '../../../models/enums/endpoint.enum';
import { CrytonRESTApiService } from '../cryton-rest-api-service';

@Injectable({
  providedIn: 'root'
})
export class WorkersService extends CrytonRESTApiService<Worker> {
  get endpoint(): string {
    return CrytonRESTApiService.buildEndpointURL(Endpoint.WORKERS);
  }

  constructor(protected http: HttpClient) {
    super(http);
  }

  healthCheck(workerID: number): Observable<WorkerHealth> {
    return this.http.post<WorkerHealth>(`${this.endpoint}${workerID}/healthcheck/`, {});
  }
}
