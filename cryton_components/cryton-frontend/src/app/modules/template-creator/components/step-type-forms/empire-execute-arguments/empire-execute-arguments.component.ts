import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SelectionOption } from 'src/app/shared/models/interfaces/selection-option.interface';
import { parse, stringify } from 'yaml';
import { yamlValidator } from '../../../classes/utils/validate-yaml';
import { EmpireExecuteArguments } from '../../../models/interfaces/step-arguments/empire-execute-arguments';
import { StepArgumentsComponent } from '../../../models/interfaces/step-arguments/step-arguments-component';
import { StepArgumentsType } from '../../../models/types/step-node.type';

enum EmpireExecuteArgumentsType {
  SHELL_COMMAND,
  EMPIRE_MODULE
}

@Component({
  selector: 'app-empire-execute-arguments',
  templateUrl: './empire-execute-arguments.component.html',
  styleUrls: ['../../../styles/step-form.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmpireExecuteArgumentsComponent implements StepArgumentsComponent<StepArgumentsType> {
  readonly form = new FormGroup({
    use_agent: new FormControl<string>(null, [Validators.required]),
    shellCmdForm: new FormGroup({
      shell_command: new FormControl<string>(null, [Validators.required])
    }),
    empireModuleForm: new FormGroup({
      module: new FormControl<string>(null, [Validators.required]),
      module_arguments: new FormControl<string>(null, { validators: [yamlValidator()], updateOn: 'blur' })
    })
  });

  readonly argsTypeOptions: SelectionOption<EmpireExecuteArgumentsType>[] = [
    { value: EmpireExecuteArgumentsType.SHELL_COMMAND, name: 'Shell command' },
    { value: EmpireExecuteArgumentsType.EMPIRE_MODULE, name: 'Empire module' }
  ];

  readonly argsTypeCtrl = new FormControl(null, [Validators.required]);
  readonly EmpireExecuteArgumentsType = EmpireExecuteArgumentsType;

  constructor(private _cd: ChangeDetectorRef) {}

  get shellCmdForm(): FormGroup {
    return this.form.controls.shellCmdForm;
  }
  get empireModuleForm(): FormGroup {
    return this.form.controls.empireModuleForm;
  }

  isValid(): boolean {
    return this.form.valid && this.argsTypeCtrl.valid;
  }

  fill(args: EmpireExecuteArguments): void {
    const { module_arguments, module, shell_command, use_agent } = args;

    if ((module_arguments || module) && shell_command) {
      throw new Error(`Invalid arguments, can't use both shell command and empire module.`);
    }

    this.form.get('use_agent').setValue(use_agent);

    if (shell_command) {
      this.argsTypeCtrl.setValue(EmpireExecuteArgumentsType.SHELL_COMMAND);
      this.onArgsTypeChange(EmpireExecuteArgumentsType.SHELL_COMMAND);
      this._cd.detectChanges();

      this.shellCmdForm.get('shell_command').setValue(shell_command);
    } else {
      this.argsTypeCtrl.setValue(EmpireExecuteArgumentsType.EMPIRE_MODULE);
      this.onArgsTypeChange(EmpireExecuteArgumentsType.EMPIRE_MODULE);
      this._cd.detectChanges();

      this.empireModuleForm.patchValue({
        module_arguments: module_arguments ? stringify(module_arguments) : undefined,
        module
      });
    }
  }

  getArguments(): EmpireExecuteArguments {
    const use_agent = this.form.get('use_agent').value;

    if (this.argsTypeCtrl.value == null) {
      throw Error(`Arguments type haven't been chosen yet.`);
    } else if (this.argsTypeCtrl.value === EmpireExecuteArgumentsType.SHELL_COMMAND) {
      const shell_command = this.shellCmdForm.get('shell_command').value;
      return { use_agent, shell_command };
    } else {
      const { module, module_arguments } = this.empireModuleForm.value;
      return { use_agent, module, module_arguments: module_arguments ? parse(module_arguments) : undefined };
    }
  }

  onArgsTypeChange(type: EmpireExecuteArgumentsType): void {
    if (type === EmpireExecuteArgumentsType.SHELL_COMMAND) {
      this.shellCmdForm.enable();
      this.empireModuleForm.disable();
    } else {
      this.shellCmdForm.disable();
      this.empireModuleForm.enable();
    }
  }
}
