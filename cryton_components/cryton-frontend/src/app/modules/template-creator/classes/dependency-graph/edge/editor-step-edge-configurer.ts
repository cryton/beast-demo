import Konva from 'konva';
import { Arrow } from 'konva/lib/shapes/Arrow';
import { Cursor } from '../cursor-state';
import { DependencyGraphEditor } from '../dependency-graph-editor';
import { RegularEdgeEventConfigurer } from './regular-edge-event-configurer';
import { RegularKonvaGraphEdge } from './regular-konva-graph-edge';
import { StepEdge } from './step-edge';

export class EditorStepEdgeConfigurer implements RegularEdgeEventConfigurer {
  constructor(
    private _editor: DependencyGraphEditor,
    private _edge: StepEdge,
    private _konvaGraphEdge: RegularKonvaGraphEdge
  ) {}

  configure(arrow: Konva.Arrow): void {
    this.removeOwnListeners(arrow);

    arrow.on('mouseenter.editor-step-edge', () => {
      this._onMouseEnter();
    });
    arrow.on('mouseleave.editor-step-edge', () => {
      this._onMouseLeave();
    });
    arrow.on('click.editor-step-edge', () => {
      this._onClick();
    });
  }

  removeOwnListeners(arrow: Arrow): void {
    arrow.off('mouseenter.editor-step-edge');
    arrow.off('mouseleave.editor-step-edge');
    arrow.off('click.editor-step-edge');
  }

  removeAllListeners(arrow: Konva.Arrow): void {
    arrow.off();
  }

  private _onMouseEnter(): void {
    this._editor.cursorState.setCursor(Cursor.POINTER);

    if (this._isDeleteEnabled()) {
      this._konvaGraphEdge.activateStroke(this._editor.theme.primary, true);
    }
  }

  private _onMouseLeave(): void {
    this._editor.cursorState.unsetCursor(Cursor.POINTER);
    this._konvaGraphEdge.setTheme(this._editor.theme);
  }

  private _onClick(): void {
    if (this._isDeleteEnabled()) {
      this._edge.destroy();
    }

    if (!this._isDeleteEnabled()) {
      this._editor.editStepEdge$.next(this._edge);
    }
  }

  protected _isDeleteEnabled(): boolean {
    return this._editor.toolState.isDeleteEnabled;
  }
}
