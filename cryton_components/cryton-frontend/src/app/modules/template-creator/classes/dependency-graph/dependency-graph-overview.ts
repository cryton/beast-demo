import Konva from 'konva';
import { Vector2d } from 'konva/lib/types';
import { Observable, Subject } from 'rxjs';
import { TooltipOrientation } from 'src/app/shared/components/cryton-tooltip/cryton-tooltip.component';
import { TooltipPositioner } from 'src/app/shared/utils/tooltip-positioner';
import { NodeType } from '../../models/enums/node-type';
import { StageDescription, StepDescription } from '../../models/interfaces/template-description';
import { KonvaWrapper } from '../konva/konva-wrapper';
import { DependencyGraph } from './dependency-graph';
import { EventConfigurer } from './event-configurer';
import { StageNode } from './node/stage-node';
import { StepNode } from './node/step-node';

export interface TooltipData {
  x: number;
  y: number;
  orientation: TooltipOrientation;
  nodeType: NodeType;
  data: StageDescription | StepDescription;
}

export class DependencyGraphOverview extends KonvaWrapper {
  graphLayer = new Konva.Layer();
  depGraph: DependencyGraph;

  displayTooltip$: Observable<TooltipData | undefined>;

  private _displayTooltip$ = new Subject<TooltipData | undefined>();
  private _tooltipPositioner = new TooltipPositioner();

  constructor() {
    super();
    this.depGraph = new DependencyGraph(this.theme);
    this.displayTooltip$ = this._displayTooltip$.asObservable();
  }

  changeDepGraph(depGraph: DependencyGraph): void {
    this.depGraph = depGraph;
    depGraph.wrapper = this;

    if (depGraph.getContainer()) {
      this.graphLayer = depGraph.getContainer() as Konva.Layer;

      if (this.stage) {
        this.stage.removeChildren();
        this.stage.add(this.graphLayer);
      }
    } else {
      depGraph.draw(this.graphLayer, false);
    }
  }

  draw(): void {
    this.depGraph.draw(this.graphLayer, true);
  }

  configureEvents(configurer: EventConfigurer) {
    this.depGraph.nodes.forEach(node => node.configureEvents(configurer));
  }

  displayTooltip(node: StageNode | StepNode, { x, y }: Vector2d): void {
    const orientation = this._tooltipPositioner.calcOrientation({ width: this.width, height: this.height }, { x, y }, [
      'middle',
      'top'
    ]);

    this._displayTooltip$.next({
      x,
      y,
      orientation,
      nodeType: node instanceof StageNode ? NodeType.CRYTON_STAGE : NodeType.CRYTON_STEP,
      data: node.getYaml()
    });
  }

  /**
   * Initializes stage container and dimensions.
   *
   * @param container Container element.
   */
  protected _createStage(container: HTMLDivElement): void {
    const { width, height } = this._getBoundingRect(container);

    this.stage = new Konva.Stage({
      container,
      width,
      height,
      draggable: true
    });

    this.stage.add(this.graphLayer);

    this.stage.on('click', evt => {
      if (evt.target === this.stage) {
        this._displayTooltip$.next(undefined);
      }
    });
  }

  protected _refreshTheme(): void {
    this.depGraph.updateTheme(this.theme);
  }
}
