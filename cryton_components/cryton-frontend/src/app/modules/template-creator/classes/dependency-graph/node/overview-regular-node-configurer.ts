import Konva from 'konva';
import { Cursor } from '../cursor-state';
import { DependencyGraphOverview } from '../dependency-graph-overview';
import { NodeConnector } from '../node-connector';
import { GraphNode } from './graph-node';
import { RegularNodeEventConfigurer } from './regular-node-event-configurer';
import { StepNode } from './step-node';

export class OverviewRegularNodeConfigurer implements RegularNodeEventConfigurer {
  constructor(private _wrapper: DependencyGraphOverview) {}

  configure(
    node: GraphNode,
    primaryText: Konva.Text,
    secondaryText: Konva.Text,
    rectangle: Konva.Rect,
    connector: NodeConnector
  ): void {
    this.removeOwnListeners(node, primaryText, secondaryText, rectangle, connector);

    rectangle.on('mouseenter.overview-regular-node', () => {
      this._wrapper.cursorState.setCursor(Cursor.POINTER);
    });

    rectangle.on('mouseleave.overview-regular-node', () => {
      this._wrapper.cursorState.unsetCursor(Cursor.POINTER);
    });

    node.konvaGraphNode.getKonvaObject().on('click.overview-regular-node', ({ evt }) => {
      this._wrapper.displayTooltip(node as StepNode, { x: evt.offsetX, y: evt.offsetY });
    });
  }

  removeOwnListeners(
    node: GraphNode,
    primaryText: Konva.Text,
    secondaryText: Konva.Text,
    rectangle: Konva.Rect,
    connector: NodeConnector
  ): void {
    rectangle.off('mouseenter.overview-regular-node');
    rectangle.off('mouseleave.overview-regular-node');
    node.konvaGraphNode.getKonvaObject().off('click.overview-regular-node');
  }

  removeAllListeners(
    node: GraphNode,
    primaryText: Konva.Text,
    secondaryText: Konva.Text,
    rectangle: Konva.Rect,
    connector: NodeConnector
  ): void {
    node.konvaGraphNode.getKonvaObject().off();
    primaryText.off();
    secondaryText.off();
    rectangle.off();
    connector.getKonvaObject().off();
  }
}
