import { TriggerArgs } from '../../classes/triggers/trigger';
import { StepNodeType } from '../enums/step-node-type.enum';
import { TriggerType } from '../enums/trigger-type';
import { StepArgumentsType } from '../types/step-node.type';
import { OutputMapping } from './output-mapping';

/**
 * Plan template detail structure.
 */
export interface TemplateDescription {
  plan: PlanDescription;
}

/**
 * Plan detail structure.
 */
export interface PlanDescription {
  name: string;
  owner: string;
  stages: StageDescription[];
}

/**
 * Plan template stage.
 */
export interface StageDescription {
  depends_on?: string[];
  name: string;
  trigger_args: TriggerArgs;
  trigger_type: TriggerType;
  steps: StepDescription[];
}

/**
 * Step in stage.
 */
export interface StepDescription {
  is_init?: boolean;
  step_type: StepNodeType;
  arguments: StepArgumentsType;
  create_named_session?: string;
  use_named_session?: string;
  next?: StepEdgeDescription[];
  output_prefix?: string;
  output_mapping?: OutputMapping[];
  name: string;
}

/**
 * Edge between steps.
 */
export interface StepEdgeDescription {
  type: string;
  value?: string;
  step: string;
}
