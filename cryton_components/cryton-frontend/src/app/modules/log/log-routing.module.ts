import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListLogsPageComponent } from './pages/list-logs-page/list-logs-page.component';

const routes: Routes = [
  {
    path: '',
    component: ListLogsPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LogRoutingModule {}
