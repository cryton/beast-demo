import { Component, Type } from '@angular/core';
import { renderComponentTrigger } from 'src/app/shared/animations/render-component.animation';
import { StepOverviewItem } from 'src/app/shared/components/cryton-editor/models/step-overview-item.interface';
import { StepType } from 'src/app/shared/components/cryton-editor/models/step-type.enum';
import { WorkerCreationStepsComponent } from 'src/app/shared/components/cryton-editor/steps/worker-creation-steps/worker-creation-steps.component';

@Component({
  selector: 'app-create-worker-page',
  templateUrl: './create-worker-page.component.html',
  animations: [renderComponentTrigger]
})
export class CreateWorkerPageComponent {
  editorSteps: Type<WorkerCreationStepsComponent> = WorkerCreationStepsComponent;
  stepOverviewItems: StepOverviewItem[] = [{ name: 'Creation Progress', type: StepType.COMPLETION, required: true }];

  constructor() {}
}
