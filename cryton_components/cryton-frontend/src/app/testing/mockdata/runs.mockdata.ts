import { Run } from '../../models/api-responses/run.interface';

export const runs: Run[] = [
  {
    id: 1,
    created_at: '2020-06-29T16:45:23.615123',
    updated_at: '2020-06-29T16:45:23.615136',
    start_time: '2020-05-12T16:50:23.615136',
    schedule_time: null,
    pause_time: null,
    finish_time: null,
    state: 'SCHEDULED',
    aps_job_id: null,
    plan_model: 1
  },
  {
    id: 2,
    created_at: '2020-06-29T16:46:53.539149',
    updated_at: '2020-06-29T16:46:53.539160',
    start_time: null,
    pause_time: null,
    finish_time: null,
    schedule_time: null,
    state: 'PENDING',
    aps_job_id: null,
    plan_model: 2
  },
  {
    id: 3,
    created_at: '2020-06-29T16:47:30.765240',
    updated_at: '2020-06-29T16:47:30.765252',
    start_time: null,
    pause_time: null,
    finish_time: null,
    schedule_time: null,
    state: 'RUNNING',
    aps_job_id: null,
    plan_model: 3
  },
  {
    id: 4,
    created_at: '2020-06-29T16:47:38.188259',
    updated_at: '2020-06-29T16:47:38.188270',
    start_time: null,
    pause_time: null,
    finish_time: null,
    schedule_time: null,
    state: 'PENDING',
    aps_job_id: null,
    plan_model: 4
  },
  {
    id: 5,
    created_at: '2020-06-29T16:47:48.137229',
    updated_at: '2020-06-29T16:47:48.137241',
    start_time: '2020-06-29T16:47:48.137229',
    pause_time: null,
    finish_time: null,
    schedule_time: null,
    state: 'FINISHED',
    aps_job_id: null,
    plan_model: 1
  },
  {
    id: 6,
    created_at: '2020-06-29T16:48:06.314021',
    updated_at: '2020-06-29T16:48:06.314032',
    start_time: null,
    pause_time: null,
    finish_time: null,
    schedule_time: null,
    state: 'PAUSING',
    aps_job_id: null,
    plan_model: 1
  },
  {
    id: 7,
    created_at: '2020-06-29T16:48:15.810439',
    updated_at: '2020-06-29T16:48:15.810451',
    start_time: null,
    pause_time: null,
    finish_time: null,
    schedule_time: null,
    state: 'PAUSED',
    aps_job_id: null,
    plan_model: 3
  },
  {
    id: 8,
    created_at: '2020-06-29T16:48:20.064168',
    updated_at: '2020-06-29T16:48:20.064178',
    start_time: null,
    pause_time: null,
    finish_time: null,
    schedule_time: null,
    state: 'PENDING',
    aps_job_id: null,
    plan_model: 2
  },
  {
    id: 9,
    created_at: '2020-07-05T10:07:05.800595',
    updated_at: '2020-07-05T10:07:05.800607',
    start_time: null,
    pause_time: null,
    finish_time: null,
    schedule_time: null,
    state: 'FINISHED',
    aps_job_id: null,
    plan_model: 1
  },
  {
    id: 10,
    created_at: '2020-07-05T10:07:05.800595',
    updated_at: '2020-07-05T10:07:05.800607',
    start_time: null,
    pause_time: null,
    finish_time: null,
    schedule_time: null,
    state: 'FINISHED',
    aps_job_id: null,
    plan_model: 1
  }
];
