import Konva from 'konva';
import { Theme } from '../../../models/interfaces/theme';
import { Trigger, TriggerArgs } from '../../triggers/trigger';
import { DependencyGraph } from '../dependency-graph';
import { GraphEdge } from '../edge/graph-edge';

export interface CrytonStageGraphConfig {
  name: string;
  childDepGraph: DependencyGraph;
  trigger: Trigger<TriggerArgs>;
}

export class StageGraphNode {
  name: string;
  note?: string;
  state?: string;
  childEdges: GraphEdge[] = [];
  parentEdges: GraphEdge[] = [];
  konvaObject = new Konva.Group();
  trigger: Trigger<TriggerArgs>;

  private _nodeRect: Konva.Rect;
  private _childDepGraph: DependencyGraph;

  constructor(config: CrytonStageGraphConfig) {
    this.name = config.name;
    this.note = this.trigger.getType();
    this._childDepGraph = config.childDepGraph;
  }

  changeTheme: (theme: Theme) => void;
}
