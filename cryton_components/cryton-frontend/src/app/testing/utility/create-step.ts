import { DependencyGraph } from 'src/app/modules/template-creator/classes/dependency-graph/dependency-graph-editor';
import { StepArguments, StepNode } from 'src/app/modules/template-creator/classes/dependency-graph/node/step-node';

export const createStep = (args: StepArguments, depGraph: DependencyGraph): StepNode => {
  const step = new StepNode(args);
  step.setParentDepGraph(depGraph);
  return step;
};
