/**
 * Expected description of main template dependency graph.
 */
export const basicTemplateDescription = `plan:
  name: Basic template
  owner: Test runner
  stages:
    - name: stage-one
      trigger_type: delta
      trigger_args:
        hours: 1
        minutes: 20
        seconds: 20
      steps:
        - name: scan-localhost
          step_type: worker/execute
          arguments:
            module: mod_nmap
            module_arguments:
              target: 127.0.0.1
              ports:
                - 22
          is_init: true
          next:
            - step: bruteforce
              type: result
              value: OK
        - name: bruteforce
          step_type: worker/execute
          arguments:
            module: mod_medusa
            module_arguments:
              target: 127.0.0.1
              credentials:
                username: vagrant
`;
