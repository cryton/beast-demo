import { ComponentHarness } from '@angular/cdk/testing';

export class WorkerTableHarness extends ComponentHarness {
  static hostSelector = 'app-worker-table';

  private _getStateHeader = this.locatorFor('.worker__state-name');
  private _getIDHeader = this.locatorFor('.worker__id');
  private _getNameParagraph = this.locatorFor('#name-text');
  private _getDescriptionParagraph = this.locatorFor('#description-text');

  async getState(): Promise<string> {
    const state = await this._getStateHeader();
    return state.text();
  }

  async getID(): Promise<number> {
    const id = await this._getIDHeader();
    const idText = await id.text();

    // Slice off the "ID: " at the beginning
    return Number(idText.slice(3));
  }

  async getName(): Promise<string> {
    const nameParagraph = await this._getNameParagraph();
    return nameParagraph.text();
  }

  async getDescription(): Promise<string> {
    const descriptionParagraph = await this._getDescriptionParagraph();
    return descriptionParagraph.text();
  }
}
