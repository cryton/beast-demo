from django.apps import AppConfig


class CrytonCoreAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cryton_core.cryton_app'
