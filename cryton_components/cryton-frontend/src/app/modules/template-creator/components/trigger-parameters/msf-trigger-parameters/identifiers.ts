export const identifiers = [
  'type',
  'tunnel_local',
  'tunnel_peer',
  'via_exploit',
  'via_payload',
  'desc',
  'info',
  'workspace',
  'session_host',
  'session_port',
  'target_host',
  'username',
  'uuid',
  'exploit_uuid',
  'routes',
  'arch'
];
