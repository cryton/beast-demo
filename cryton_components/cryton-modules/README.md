[[_TOC_]]

# Cryton Modules

## Description
Cryton (attack) modules is a collection of Python scripts with the goal of orchestrating known offensive security tools 
(Nmap, Metasploit, THC Hydra, etc.). Although this is their intended purpose, they are still Python scripts, and therefore 
any custom-made script can be used similarly.

Attack modules get executed inside Step objects using [Cryton Worker](https://gitlab.ics.muni.cz/beast-public/cryton/cryton-worker). 
All provided arguments are given to the Attack module.

Cryton toolset is tested and targeted primarily on **Debian** and **Kali Linux**. Please keep in mind that 
**only the latest version is supported** and issues regarding different OS or distributions may **not** be resolved.

[Link to the documentation](https://beast-public.gitlab-pages.ics.muni.cz/cryton/cryton-documentation/).

## Usage
**Before you download the modules or once you clone the repository, please make sure to choose the correct version (`git checkout <version>`).**

As mentioned, modules are primarily targeted for use with Cryton Worker. See how to set them up [here](https://gitlab.ics.muni.cz/beast-public/cryton/cryton-worker#setting-up-modules).

Since they are python modules, you can install and use them manually. You should use a virtual environment like [Poetry](https://python-poetry.org/docs/).

## Creating modules
In this section, we will discuss best practices and some rules that each module must follow.

Here's an example of a typical module directory:
```
my_module_name/
├── mod.py
├── test_mod.py
├── README.md
├── requirements.txt
└── example.py
```

### mod.py
The most important file is the module itself (**must be called `mod.py`**). It consists of two main methods:
- `execute` (is used as an entry point for module execution; takes and returns **dictionary**)
- `validate` (is used to validate input parameters for the `execute` method; takes **dictionary** and returns 0 if it's okay, else raises an exception)

The [input](https://gitlab.ics.muni.cz/beast-public/cryton/cryton-worker#input-parameters) 
and [output](https://gitlab.ics.muni.cz/beast-public/cryton/cryton-worker#output-parameters) are specified in Worker.

You can also use [prebuilt functionality](https://gitlab.ics.muni.cz/beast-public/cryton/cryton-worker#prebuilt-functionality-for-modules) from Worker.

Here's a simple example:
```python
def validate(arguments: dict) -> int:
    if arguments != {}:
        return 0  # If arguments are valid.
    raise Exception("No arguments")  # If arguments aren't valid.

def execute(arguments: dict) -> dict:
    # Do stuff.
    return {"return_code": 0, "serialized_output": ["x", "y"]}
```


### test_mod.py
Contains a set of tests to check if the code is correct.

Here's a simple example:
```python
from mod import execute


class TestMyModuleName:
    def test_mod_execute(self):
        arguments = {'cmd': "test"}

        result = execute(arguments)

        assert result == {"return_code": 0}

```

### README.md
README file should describe what the module is for, and its IO parameters, and give the user some examples.

It should also say what system requirements are necessary (with version).

### requirements.txt
Here are specified Python packages that are required to run the module. These requirements must be compliant with the
Python requirements in Cryton Worker.

For example, if the module wants to use the `schema` package with version *2.0.0*, but the Worker requires version *2.1.1*, it won't work.

### example.py
Is a set of predefined parameters that should allow the user to test if the module works as intended.

Example:

```python
from mod import execute, validate

args = {
    "argument1": "value1",
    "argument2": "value2"
}

validate_output = validate(args)
print(f"validate output: {validate_output}")

execute_output = execute(args)
print(f"execute output: {execute_output}")


```
