import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable, of, throwError } from 'rxjs';
import { catchError, first, mapTo, mergeMap, pluck } from 'rxjs/operators';
import { PlanExecution } from 'src/app/models/api-responses/plan-execution.interface';
import { Report } from 'src/app/models/api-responses/report/report.interface';
import { Run } from 'src/app/models/api-responses/run.interface';
import { Endpoint } from 'src/app/models/enums/endpoint.enum';
import { HasYaml } from 'src/app/models/interfaces/has-yaml.interface';
import { CrytonRESTApiService } from '../cryton-rest-api-service';
import { ExecutionVariableService } from '../execution-variable/execution-variable.service';

export interface RunResponse {
  id: number;
  detail: string;
  plan_execution_ids: number[];
}

@Injectable({
  providedIn: 'root'
})
export class RunService extends CrytonRESTApiService<Run> implements HasYaml {
  get endpoint(): string {
    return CrytonRESTApiService.buildEndpointURL(Endpoint.RUNS);
  }

  constructor(protected http: HttpClient, private _execVarService: ExecutionVariableService) {
    super(http);
  }

  scheduleRun(date: Date, runID: number): Observable<string> {
    const body = { start_time: this._formatDate(date) };

    return this._runAction(runID, 'schedule', body).pipe(
      mapTo(`Run with ID: ${runID} scheduled successfully.`),
      catchError((err: HttpErrorResponse) => this.handleItemError(err, `Scheudling run with ID: ${runID} failed.`))
    );
  }

  rescheduleRun(date: Date, runID: number): Observable<string> {
    const body = { start_time: this._formatDate(date) };

    return this._runAction(runID, 'reschedule', body).pipe(
      mapTo(`Run with ID: ${runID} rescheduled successfully.`),
      catchError((err: HttpErrorResponse) => this.handleItemError(err, `Rescheduling run with ID: ${runID} failed.`))
    );
  }

  unscheduleRun(runID: number): Observable<string> {
    return this._runAction(runID, 'unschedule').pipe(
      mapTo(`Run with ID: ${runID} unscheduled successfully.`),
      catchError((err: HttpErrorResponse) => this.handleItemError(err, `Unscheduling run with ID: ${runID} failed.`))
    );
  }

  pauseRun(runID: number): Observable<string> {
    return this._runAction(runID, 'pause').pipe(
      mapTo(`Run with ID: ${runID} paused successfully.`),
      catchError((err: HttpErrorResponse) => this.handleItemError(err, `Pausing run with ID: ${runID} failed.`))
    );
  }

  unpauseRun(runID: number): Observable<string> {
    return this._runAction(runID, 'unpause').pipe(
      mapTo(`Run with ID: ${runID} unpaused successfully.`),
      catchError((err: HttpErrorResponse) => this.handleItemError(err, `Unpausing run with ID: ${runID} failed.`))
    );
  }

  executeRun(runID: number): Observable<string> {
    return this._runAction(runID, 'execute').pipe(
      mapTo(`Run with ID: ${runID} started successfully.`),
      catchError((err: HttpErrorResponse) => this.handleItemError(err, `Execution of run with ID: ${runID} failed.`))
    );
  }

  killRun(runID: number): Observable<string> {
    return this._runAction(runID, 'kill').pipe(
      mapTo(`Run with ID: ${runID} killed successfully.`),
      catchError((err: HttpErrorResponse) => this.handleItemError(err, `Failed to kill run with ID: ${runID}.`))
    );
  }

  postponeRun(runID: number, delta: string): Observable<string> {
    return this._runAction(runID, 'postpone', { delta }).pipe(
      mapTo(`Run with ID: ${runID} postponed successfully.`),
      catchError((err: HttpErrorResponse) => this.handleItemError(err, `Failed to postpone run with ID: ${runID}.`))
    );
  }

  /**
   * Posts a run and uploads execution variables for each plan execution.
   *
   * @param body Run body.
   * @param variables Record with keys as worker ids and values as execution variables.
   * @returns Observable of alert message.
   */
  postRun(body: Record<string, unknown>, variables: Record<number, File[] | string>): Observable<string> {
    const planExecutionBaseUrl = CrytonRESTApiService.buildEndpointURL(Endpoint.PLAN_EXECUTIONS);

    return this.http.post<RunResponse>(this.endpoint, body).pipe(
      pluck('plan_execution_ids'),
      mergeMap((ids: number[]) => {
        if (variables) {
          const variableRequests = ids.map(id =>
            this.http
              .get<PlanExecution>(`${planExecutionBaseUrl}${id}/`)
              .pipe(mergeMap((execution: PlanExecution) => this.postVariables(execution, variables)))
          );
          return forkJoin(variableRequests);
        }
        return of();
      }),
      mapTo('Run created successfully.'),
      catchError((err: HttpErrorResponse) => this.handleItemError(err, 'Run creation failed.'))
    );
  }

  postVariables(execution: PlanExecution, variables: Record<number, File[] | string>): Observable<string> {
    const workersVariables = variables[execution.worker];

    if (workersVariables && workersVariables.length > 0) {
      let variablesObservable: Observable<string>;

      if (Array.isArray(workersVariables)) {
        variablesObservable = this._execVarService.postVariablesFiles(execution.id, workersVariables);
      } else {
        variablesObservable = this._execVarService.postVariablesYaml(execution.id, workersVariables);
      }
      return variablesObservable.pipe(
        catchError(() => throwError(() => new Error('Run created but failed to upload execution variables.')))
      );
    }
    return of('');
  }

  /**
   * Fetches report from a run with giver run ID.
   *
   * @param runID ID of a run.
   * @returns Observable of the report from a run.
   */
  fetchReport(runID: number): Observable<Report> {
    return this.http.get<{ detail: Report }>(`${this.endpoint}${runID}/report`).pipe(pluck('detail'));
  }

  fetchYaml(runID: number): Observable<Record<string, unknown>> {
    return this.http.get<Record<string, unknown>>(`${this.endpoint}${runID}/get_plan`);
  }

  downloadReport(runID: number): void {
    this.fetchReport(runID)
      .pipe(first())
      .subscribe(report => {
        const blob = new Blob([JSON.stringify(report)], { type: 'application/json' });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = url;
        link.download = 'report.json';
        link.click();
        link.remove();
      });
  }

  private _runAction(
    runID: number,
    action: string,
    body: Record<string, unknown> = {}
  ): Observable<Record<string, unknown>> {
    const runUrl = `${this.endpoint}${runID}/${action}/`;

    return this.http.post<Record<string, unknown>>(runUrl, body);
  }

  /**
   * Converts date to format accepted by the backend.
   *
   * @param date Date to convert.
   * @returns Converted date string.
   */
  private _formatDate(date: Date): string {
    return date.toISOString();
  }
}
