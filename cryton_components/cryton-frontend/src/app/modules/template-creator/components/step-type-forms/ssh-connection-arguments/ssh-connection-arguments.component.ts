import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SshConnectionArguments } from '../../../models/interfaces/step-arguments/ssh-connection-arguments';
import { StepArgumentsComponent } from '../../../models/interfaces/step-arguments/step-arguments-component';

@Component({
  selector: 'app-ssh-connection-arguments',
  templateUrl: './ssh-connection-arguments.component.html',
  styleUrls: ['../../../styles/step-form.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SshConnectionArgumentsComponent implements StepArgumentsComponent<SshConnectionArguments> {
  readonly form = new FormGroup({
    target: new FormControl<string>(null, [Validators.required]),
    username: new FormControl<string>(null),
    password: new FormControl<string>(null),
    ssh_key: new FormControl<string>(null),
    port: new FormControl<number>(null, [Validators.min(0), Validators.max(65535), Validators.pattern(/^[0-9]*$/)])
  });

  constructor() {}

  getArguments(): SshConnectionArguments {
    const { port, ...restOfTheArgs } = this.form.getRawValue();
    return { port: port ? Number(port) : undefined, ...restOfTheArgs };
  }

  fill(args: SshConnectionArguments): void {
    this.form.reset(args);
  }

  isValid(): boolean {
    return this.form.valid;
  }
}
