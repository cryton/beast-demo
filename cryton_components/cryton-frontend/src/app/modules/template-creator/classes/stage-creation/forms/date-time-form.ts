import { FormControl, FormGroup } from '@angular/forms';
import { withoutUndefinedAndNull } from 'src/app/shared/utils/without-undefined';
import { DateTimeTriggerParametersComponent } from '../../../components/trigger-parameters/date-time-trigger-parameters/date-time-trigger-parameters.component';
import { DateTimeArgs } from '../../../models/interfaces/date-time-args';
import { StageNode } from '../../dependency-graph/node/stage-node';
import { DateTimeUtils } from '../utils/date-time.utils';
import { FormUtils } from './form-utils';
import { TriggerForm } from './trigger-form.interface';

export class DateTimeForm implements TriggerForm {
  formComponent = DateTimeTriggerParametersComponent;

  private _triggerArgsForm = new FormGroup({
    timezone: new FormControl<string | null>(null),
    year: new FormControl<number | null>(null),
    month: new FormControl<number | null>(null),
    day: new FormControl<number | null>(null),
    hour: new FormControl<number | null>(null),
    minute: new FormControl<number | null>(null),
    second: new FormControl<number | null>(null),
    displayDateTime: new FormControl<string | null>({ value: null, disabled: true })
  });

  constructor() {}

  getArgsForm(): FormGroup {
    return this._triggerArgsForm;
  }

  getArgs(): DateTimeArgs {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { displayDateTime, ...dateTimeArgs } = this._triggerArgsForm.value;
    return withoutUndefinedAndNull(dateTimeArgs);
  }

  isValid(): boolean {
    return this._triggerArgsForm.valid;
  }

  erase(): void {
    this._triggerArgsForm.reset();
  }

  fill(stage: StageNode): void {
    const stageTriggerArgs = stage.trigger.getArgs() as DateTimeArgs;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { timezone, ...dateArgs } = stageTriggerArgs;
    this._triggerArgsForm.patchValue(stageTriggerArgs);

    const selecteDate = DateTimeUtils.dateFromDateTimeArgs(dateArgs);

    if (selecteDate) {
      this._triggerArgsForm.get('displayDateTime').setValue(selecteDate.toLocaleString());
    }
  }

  copy(): DateTimeForm {
    const copyForm = new DateTimeForm();
    copyForm._triggerArgsForm.patchValue(this._triggerArgsForm.value);

    return copyForm;
  }

  markAsUntouched(): void {
    this._triggerArgsForm.markAsUntouched();
  }

  isNotEmpty(): boolean {
    return FormUtils.someValueOtherThan(this._triggerArgsForm.value, null);
  }
}
