import { WorkersService } from 'src/app/core/http/worker/workers.service';
import { Worker } from 'src/app/models/api-responses/worker.interface';
import { TableDataSource } from './table.datasource';

export class WorkersDashboardDataSource extends TableDataSource<Worker> {
  constructor(protected workersService: WorkersService) {
    super(workersService);
  }
}
