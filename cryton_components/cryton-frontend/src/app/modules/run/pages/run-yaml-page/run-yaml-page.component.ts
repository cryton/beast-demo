import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RunService } from 'src/app/core/http/run/run.service';
import { AlertService } from 'src/app/core/services/alert/alert.service';

@Component({
  selector: 'app-run-yaml-page',
  template:
    '<app-yaml-preview [resourceService]="runService" itemName="run" [pluckArgs]="pluckArgs" [keepLastArg]="false"></app-yaml-preview>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RunYamlPageComponent {
  pluckArgs = ['detail'];

  constructor(protected _route: ActivatedRoute, public runService: RunService, protected _alert: AlertService) {}
}
