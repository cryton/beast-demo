import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import { AlertService } from 'src/app/core/services/alert/alert.service';
import { Alert } from 'src/app/shared/models/interfaces/alert.interface';
import { DependencyGraph } from '../../classes/dependency-graph/dependency-graph';
import { DependencyGraphEditor } from '../../classes/dependency-graph/dependency-graph-editor';
import { GraphEdge } from '../../classes/dependency-graph/edge/graph-edge';
import { NavigationButton } from '../../models/interfaces/navigation-button';
import { DependencyGraphHelpComponent } from '../../pages/help-pages/dependency-graph-help/dependency-graph-help.component';
import { DependencyGraphManagerService, DepGraphRef } from '../../services/dependency-graph-manager.service';
import { TcRoutingService } from '../../services/tc-routing.service';
import { TemplateCreatorStateService } from '../../services/template-creator-state.service';
import { EdgeParametersComponent } from '../edge-parameters/edge-parameters.component';

@Component({
  selector: 'app-dependency-graph-editor',
  templateUrl: './dependency-graph-editor.component.html',
  styleUrls: ['./dependency-graph-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DependencyGraphEditorComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() depGraphRef: DepGraphRef = DepGraphRef.STAGE_CREATION;
  @Input() navigationButtons?: NavigationButton[];
  @Output() navigate = new EventEmitter<string>();

  depGraph: DependencyGraph;
  graphEditor: DependencyGraphEditor;

  private _destroy$ = new Subject<void>();

  constructor(
    private _graphManager: DependencyGraphManagerService,
    private _dialog: MatDialog,
    private _alertService: AlertService,
    private _tcRouter: TcRoutingService,
    private _tcState: TemplateCreatorStateService
  ) {
    this.graphEditor = new DependencyGraphEditor(this._tcState.timeline);
  }

  get isSwapEnabled(): boolean {
    return this.graphEditor.toolState.isSwapEnabled;
  }

  get isDeleteEnabled(): boolean {
    return this.graphEditor.toolState.isDeleteEnabled;
  }

  get isMoveNodeEnabled(): boolean {
    return this.graphEditor.toolState.isMoveNodeEnabled;
  }

  ngOnInit(): void {
    this._createAlertSub();
    this._createEditEdgeSub();

    this._graphManager
      .getCurrentGraph(this.depGraphRef)
      .pipe(takeUntil(this._destroy$))
      .subscribe(depGraph => {
        this.depGraph = depGraph;
        this.graphEditor.changeDepGraph(depGraph);
      });
  }

  ngAfterViewInit(): void {
    this._createDepGraphSub();
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  showHelp(): void {
    this._dialog.open(DependencyGraphHelpComponent, { width: '60%' });
  }

  navigateTo(componentName: string): void {
    this.navigate.emit(componentName);
  }

  navigateToNodeEditor(): void {
    if (this.depGraphRef === DepGraphRef.STAGE_CREATION) {
      this._tcRouter.navigateTo(1);
    } else {
      this._tcRouter.navigateTo(2, 'stage_params');
    }
  }

  /**
   * Subscribes to editNode observable in current dependency graph.
   */
  private _createDepGraphSub(): void {
    this._graphManager
      .getCurrentGraph(this.depGraphRef)
      .pipe(
        switchMap(depGraph => depGraph.editNode$),
        takeUntil(this._destroy$)
      )
      .subscribe(node => {
        if (node) {
          this.navigateToNodeEditor();
        }
      });
  }

  /**
   * Subscribes to the edit cryton step edge subject
   *
   * On every next():
   * - Opens edge parameters dialog window.
   */
  private _createEditEdgeSub(): void {
    this.graphEditor.editStepEdge$.pipe(takeUntil(this._destroy$)).subscribe((edge: GraphEdge) => {
      this._dialog.open(EdgeParametersComponent, { width: '90%', maxWidth: '600px', data: { edge } });
    });
  }

  /**
   * Subscribes to alert observable from the current dependency graph.
   *
   * On every next:
   * - Emits new alert from the alert service.
   */
  private _createAlertSub(): void {
    this.graphEditor.alert$.pipe(takeUntil(this._destroy$)).subscribe((alert: Alert) => {
      switch (alert.type) {
        case 'error':
          this._alertService.showError(alert.message);
          break;
        case 'warning':
          this._alertService.showWarning(alert.message);
          break;
        case 'success':
          this._alertService.showSuccess(alert.message);
          break;
        default:
          throw new Error('Invalid alert type.');
      }
    });
  }
}
