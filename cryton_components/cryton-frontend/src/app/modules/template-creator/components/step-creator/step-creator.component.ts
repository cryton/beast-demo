import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { debounceTime, filter, takeUntil } from 'rxjs/operators';
import { AlertService } from 'src/app/core/services/alert/alert.service';
import { ComponentInputDirective } from 'src/app/shared/directives/component-input.directive';
import { SelectionOption } from 'src/app/shared/models/interfaces/selection-option.interface';
import { DependencyGraph } from '../../classes/dependency-graph/dependency-graph';
import { StepArguments, StepNode } from '../../classes/dependency-graph/node/step-node';
import { StepNodeType } from '../../models/enums/step-node-type.enum';
import { CreateStageRoute } from '../../models/enums/tc-routes.enum';
import { StepArgumentsComponent } from '../../models/interfaces/step-arguments/step-arguments-component';
import { StepArgumentsType } from '../../models/types/step-node.type';
import { StepCreatorHelpComponent } from '../../pages/help-pages/step-creator-help/step-creator-help.component';
import { DependencyGraphManagerService, DepGraphRef } from '../../services/dependency-graph-manager.service';
import { TcRoutingService } from '../../services/tc-routing.service';
import { TemplateCreatorStateService } from '../../services/template-creator-state.service';
import { EmpireAgentDeployArgumentsComponent } from '../step-type-forms/empire-agent-deploy-arguments/empire-agent-deploy-arguments.component';
import { EmpireExecuteArgumentsComponent } from '../step-type-forms/empire-execute-arguments/empire-execute-arguments.component';
import { WorkerExecuteArgumentsComponent } from '../step-type-forms/worker-execute-arguments/worker-execute-arguments.component';

export const CREATION_MSG_TIMEOUT = 7000;
export const DEFAULT_STEP_TYPE = StepNodeType.WORKER_EXECUTE;

type OutputMappingForm = FormGroup<{ name_from: FormControl<string | null>; name_to: FormControl<string | null> }>;

@Component({
  selector: 'app-step-creator',
  templateUrl: './step-creator.component.html',
  styleUrls: ['./step-creator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StepCreatorComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(ComponentInputDirective, { static: true }) typeArgumentsContainer: ComponentInputDirective;

  stepForm = new FormGroup({
    name: new FormControl<string>(null, [Validators.required]),
    type: new FormControl(DEFAULT_STEP_TYPE, [Validators.required]),
    output_prefix: new FormControl<string>(null, [Validators.pattern(/^[^\$\.]*$/)]),
    output_mapping: new FormArray<OutputMappingForm>([])
  });

  editedStep: StepNode;
  showCreationMessage$: Observable<boolean>;
  stepTypes: SelectionOption<StepNodeType>[] = [
    {
      value: StepNodeType.WORKER_EXECUTE,
      name: 'worker/execute'
    },
    {
      value: StepNodeType.EMPIRE_EXECUTE,
      name: 'empire/execute'
    },
    {
      value: StepNodeType.EMPIRE_AGENT_DEPLOY,
      name: 'empire/agent-deploy'
    }
  ];

  stepArgumentsRef: ComponentRef<Component & StepArgumentsComponent<StepArgumentsType>>;

  private _depGraph: DependencyGraph;
  private _destroy$ = new Subject<void>();
  private _stepArgsBackup: StepArguments;
  private _showCreationMessage$ = new BehaviorSubject<boolean>(false);

  constructor(
    private _graphManager: DependencyGraphManagerService,
    private _alertService: AlertService,
    private _dialog: MatDialog,
    private _tcRouter: TcRoutingService,
    private _cd: ChangeDetectorRef,
    private _tcState: TemplateCreatorStateService
  ) {
    this.showCreationMessage$ = this._showCreationMessage$.asObservable();
  }

  get stepArguments(): Omit<StepArguments, 'theme'> {
    return { type_arguments: this.getTypeArguments(), ...this.stepForm.getRawValue() };
  }

  ngOnInit(): void {
    this._createDepGraphSub();
    this._createCreationMsgSub();

    if (!this.editedStep) {
      this.restoreStepForm();
    }
  }

  ngAfterViewInit(): void {
    this._renderTypeArguments(this.stepForm.get('type').value);
    this._createEditNodeSub();
    this._createStepTypeSub();
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  /**
   * Opens help page.
   */
  openHelp(): void {
    this._dialog.open(StepCreatorHelpComponent, { width: '60%', maxWidth: '80rem' });
  }

  /**
   * Creates step and resets step form.
   */
  handleCreateStep(): void {
    if (!this.stepForm.valid) {
      this._alertService.showError('Step form is invalid.');
    } else {
      const step: StepNode = this._createStep();
      this.reset();
      this._alertService.showSuccess('Step created successfully');
      this._graphManager.addDispenserNode(DepGraphRef.STAGE_CREATION, step);
      this._showCreationMessage$.next(true);
    }
  }

  /**
   * Resets form and edited step.
   */
  cancelEditing(): void {
    this.editedStep = null;

    if (!this.restoreStepForm()) {
      this.reset();
    }
  }

  /**
   * Cancels editing and switches tab back to create stage tab.
   */
  handleCancelClick(): void {
    this._depGraph.clearEditNode();
  }

  /**
   * Saves changes to currently edited step and cancels editing.
   */
  handleSaveChanges(): void {
    this.editedStep.edit(this.stepArguments);
    this._depGraph.clearEditNode();

    // Needed to update displayed arguments in dispenser.
    this._graphManager.refreshDispenser(DepGraphRef.STAGE_CREATION);

    // If we edit step that is a part of stage that is also being edited
    // we need to navigate the user back to stage creation page.
    // This is because the stage needs to be also saved for the changes
    // to be reflected in the final template.
    if (this._tcState.editedStage) {
      this._tcRouter.navigateTo(2, CreateStageRoute.STAGE_PARAMS);
    } else {
      // Else navigate user back to the dependency graph where he started
      // the step editing.
      this._tcRouter.navigateTo(2, CreateStageRoute.DEP_GRAPH);
    }
  }

  /**
   * Gets errors for the create step button's tooltip.
   *
   * @returns Error string.
   */
  getTooltipErrors(): string {
    if (!this.stepForm.valid) {
      return '- Invalid step arguments.';
    }
  }

  /**
   * Backs up step form.
   */
  backupStepForm(): void {
    this._stepArgsBackup = this.stepArguments;
  }

  /**
   * Restores step form from backup, returns true if there was a backed up form value.
   */
  restoreStepForm(): boolean {
    if (this._stepArgsBackup) {
      this.fillFormWithArgs(this._stepArgsBackup);
      this._stepArgsBackup = null;
      return true;
    }
    return false;
  }

  navigateToStagesDepGraph(): void {
    this._tcRouter.navigateTo(2, CreateStageRoute.DEP_GRAPH);
  }

  /**
   * Fills out form fields with step arguments.
   *
   * @param step Step to fill arguments of.
   */
  fillFormWithStep(step: StepNode): void {
    this.fillFormWithArgs(step.args);
  }

  getTypeArguments(): StepArgumentsType {
    return this.stepArgumentsRef.instance.getArguments();
  }

  isValid(): boolean {
    return this.stepForm.valid && this.stepArgumentsRef.instance.isValid();
  }

  addMapping(nameFrom?: string, nameTo?: string): void {
    const mappingArray = this.stepForm.get('output_mapping') as FormArray;

    mappingArray.insert(
      0,
      new FormGroup({
        name_from: new FormControl(nameFrom ?? null, Validators.required),
        name_to: new FormControl(nameTo ?? null, Validators.required)
      })
    );
  }

  removeMapping(index: number): void {
    const mappingArray = this.stepForm.controls.output_mapping;
    mappingArray.removeAt(index);
  }

  reset(): void {
    this.stepForm.reset({ type: DEFAULT_STEP_TYPE });
    (this.stepForm.get('output_mapping') as FormArray).clear();
  }

  fillFormWithArgs(args: StepArguments): void {
    const { output_mapping, type_arguments, ...form_arguments } = args;

    this.reset();
    this.stepForm.patchValue(form_arguments);

    output_mapping?.forEach(mapping => this.addMapping(mapping.name_from, mapping.name_to));
    this.stepArgumentsRef.instance.fill(type_arguments);
  }

  /**
   * Initializes editing of the step passed in the argument.
   *
   * @param step Step to edit.
   */
  private _startEditing(step: StepNode): void {
    if (step !== this.editedStep) {
      if (!this.editedStep) {
        this.backupStepForm();
      }
      this.fillFormWithStep(step);
      this.editedStep = step;
    }
  }

  /**
   * Creates new cryton step from arguments in step form.
   *
   * @returns Cryton step.
   */
  private _createStep(): StepNode {
    return new StepNode(this.stepArguments);
  }

  /**
   * Subscribes to stage creation dep. graph subject in manager.
   *
   * On every next():
   * - Saves current dep. graph and node manager to a local variable.
   * - Initializes unique name validator.
   */
  private _createDepGraphSub(): void {
    this._graphManager
      .getCurrentGraph(DepGraphRef.STAGE_CREATION)
      .pipe(takeUntil(this._destroy$))
      .subscribe(depGraph => {
        this._depGraph = depGraph;
      });
  }

  private _createEditNodeSub(): void {
    this._graphManager
      .observeNodeEdit(DepGraphRef.STAGE_CREATION)
      .pipe(takeUntil(this._destroy$))
      .subscribe((step: StepNode) => {
        if (step) {
          this._startEditing(step);
        } else if (this.editedStep) {
          this.cancelEditing();
        }
      });
  }

  private _createCreationMsgSub(): void {
    this._showCreationMessage$
      .pipe(
        takeUntil(this._destroy$),
        filter(val => val),
        debounceTime(CREATION_MSG_TIMEOUT)
      )
      .subscribe(() => this._showCreationMessage$.next(false));
  }

  private _createStepTypeSub(): void {
    this.stepForm
      .get('type')
      .valueChanges.pipe(takeUntil(this._destroy$))
      .subscribe(type => this._renderTypeArguments(type));
  }

  private _renderTypeArguments(type: StepNodeType): void {
    const viewContainerRef = this.typeArgumentsContainer.viewContainerRef;

    if (viewContainerRef) {
      viewContainerRef.clear();

      switch (type) {
        case StepNodeType.EMPIRE_AGENT_DEPLOY:
          this.stepArgumentsRef = viewContainerRef.createComponent(EmpireAgentDeployArgumentsComponent);
          break;
        case StepNodeType.EMPIRE_EXECUTE:
          this.stepArgumentsRef = viewContainerRef.createComponent(EmpireExecuteArgumentsComponent);
          break;
        case StepNodeType.WORKER_EXECUTE:
          this.stepArgumentsRef = viewContainerRef.createComponent(WorkerExecuteArgumentsComponent);
          break;
        default:
          throw new Error(`Step node type (${type}) doesn't have an assigned arguments component.`);
      }
      this._cd.detectChanges();
    }
  }
}
